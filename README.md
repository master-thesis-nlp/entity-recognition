# Entity Recognition
 Repozytorium zawierające kod potrzebny do uruchomienia - wymagany `python 3.6` oraz `jupyter notebook`. Przed uruchomieniem do katalogu `src/application_data/wikidata/` należy umieścić wynik przetwarzania wikidata z repozytorium `Wikidata Processing` (można też pobrać obliczone już pliki z tego [linku](https://drive.google.com/drive/folders/1hfS1qNSFvISSxeg1Ru45PBQjcrarvb_c?usp=sharing) ). Pliki powinny wyglądać tak:
 ```bash
- src
\ - application_data
  \ - wikidata
      - events.pickle
      - familias.pickle
      - humans.pickle
      - locations.pickle
      - names.pickle
      - organisations.pickle
      - page_rank.pickle
      - subclasses.pickle
``` 
 Pozostałe pliki wymagane do uruchomienia powinny zostać pobrane automatyczne.
# Odtwarzanie wyników z pracy magisterskiej

Wyniki można w łatwy sposób odtworzyć uruchamiając odpowiedni zeszyt w folderze `src`:
* `Pobieranie danych.ipynb` - zeszyt ten musi być uruchomiony przed jakimkolwiek innym - pobiera on wymagane dane do zadania z Google Drive
* `Porównanie metod(brexit).ipynb` - odtwarzanie wyników z rodziału 4 na zbiorze brexit (zbiór treningowy to Asia Bibi)
* `Porównanie metod(nordstream).ipynb` - j.w. dla zbioru nordstream (zbiór treningowy brexit)
* `Inne.ipynb` - pozostałe obliczenia wymagane do pracy

# Zasada działania

Dokładny opis techniczny można znaleźć w pliku `opis-techniczny.md`.