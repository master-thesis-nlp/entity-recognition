# BSNLP 2019

## Uruchomienie

Wymagany python3. Uwaga! Rozwiązanie może zużywać sporo ramu (nawet 16 gb).
Po sklonowaniu repo należy uruchomić `git submodule init`, następnie `git submodule update`. 
Następnie należy zainstalować zależności `pip3 install -r requirements.txt`.
Następnie należy otworzyć przy pomocy jupytera `notebook.ipynb`.

## Porównywanie method

W 3 sekcji notebook'a znajduje się wywołanie funkcji compare_methods. Metoda zwraca listę krotek odpowidajacych wynikowi porównania. Funkcja bierze jako argumenty:

`def compare_methods(
        metric_factory=lambda: mentions_catch.MentionsCatch(),
        langs=None,
        categories=None,
        test_provider=get_separated_data_for_language,
        **methods)`

-  `metric_factory` - funkcja tworząca klasę liczącą zadaną metrykę. Metryka jest tworzony dla każdej metody z 
`methods` z osobna, a także w zależności od konfiguracji dla każdego języka. Przykładowe ustawienia to `lambda x: mentios_catch.MentionsCatch()`.
- `langs` - lista języków na których chcemy testować. Przykład: `langs=["pl", "cs", "All"]` - 
dostaniemy trzy różne wyniki (ostatni będzie zbiorczy)
- `categories` - lista kategorii, na których chcemy testować metody. Aktualnie niepotrzebny - *nie używać*!
- `test_provider` - parametr używany do ustawienia podziału danych wejściowych na uczące i testowe (np. zbiór 
testowy - teksty dotyczące Asi Bibi, zbiór uczący - teksty dotyczące Brexitu). Niżej opisałem możliwe funkcje podziału.
- `**methods` - *najistotniejszy* parametr tej funkcji. Lista konfiguracji, które chcemy przetestować. 
Każda konfiguracja to nazwa=słownik składający maksymalnie z trzech elementów - `ner`, `lemmatizer`, `identifier`. Gdy któryś z tych elementów nie zostanie podany, użyty zostanie odpowiadający solver z pliku `solvers/default.py`. 
Przykład konfiguracji: `WIKI2 = {"ner": prophecy.ner, "lemmatizer":prophecy.lemma, "identifier": wikidata2.get_id_by_label_for}`. Format wejścia/wyjścia musi być zgodny z tym opisanym w sekcji `Solvery`.

Wynik z funkcji jest listą krotek: `(język, kategoria, nazwa_metody, f1-score, precision, wielkość-próbki-precision, recall, wielkość-próbki-recall, nazwa-użytej-metryki)`. 
Zalecam wyświetlenie wyniku za pomocą funkcji `visualize.summarize_results`, przykład:

```
res = common.compare_methods(
    ORACLE = {"ner": prophecy.ner, "lemmatizer":prophecy.lemma, "identifier": prophecy.identifier}, 
    TYPE0 = {"ner": prophecy.ner, "lemmatizer":prophecy.lemma, "identifier": type0.identifier}, 
    WIKI2 = {"ner": prophecy.ner, "lemmatizer":prophecy.lemma, "identifier": wikidata2.get_id_by_label_for}, 
    langs=["All"],
    metric_factory=lambda:lea.Lea(fast=True, exact=False))
visualize.summarize_results(res[0], res[1:])
``` 

## Metryki

Typy złapanych fraz: `["PER", "PRO", "ORG", "LOC", "EVT"]`.

- metrics/mentions_catch - Plik zawiera klasę `MentionsCatch`, która posiada przełącznik w konstruktorze `lemma`. Przy ustawieniu `lemma=False`
zwracana jest metryka opisująca poprawnie złapane pary fraza-tag (np. ("Adama Mickiewicza", "PER")) w tekście. Przy ustawieniu `lemma=True` zwracana metryka odpowiada
poprawnym parom fraza-lemat (np. ("Adama Mickiewicza", "Adam Mickiewicz")). Metryka jest *case-sensitive*. Przykład użycia:
```python
res = common.compare_methods(
    ORACLE = {"ner": prophecy.ner, "lemmatizer":prophecy.lemma, "identifier": prophecy.identifier},
    metric_factory=lambda:mentions_catch.MentionsCatch(lemma=True)
)
```
- metric/lea - Plik zawiera klasę `Lea` definiującą docelową metrykę z zadania. Metryka przydatna przy badaniu skuteczności modułu `identifier` 
lub całego rozwiązania. Klasa w konstruktorze posiada przełącznik `exact`. Domyślna wartość to true - definicja zgodna z paperem.
Gdy jest ustawiony na false, jest to zwykła metryka precision/recall na zbiorze krawędzi pomiędzy frazami o tym samym
identyfikatorze (signletony nie posiadają krawędzi). Przykład użycia:
```python
res = common.compare_methods(
    ORACLE = {"ner": prophecy.ner, "lemmatizer":prophecy.lemma, "identifier": prophecy.identifier},
    metric_factory=lambda:lea.Lea(remove_repetition=True, exact=False)
)

```
- metrics/category_composite - klasa pomocnicza wrapująca metryki. Nie powinna być używana (framework korzysta z niej wewnętrzne)

## Solvery - dostępne

- solvers.prophecy - solver zwracający w 100% poprawny wynik z danych testowych (czasem ze względów niejednoznaczności 
zwraca trochę mniej dokładny wynik). Posiada metody: `ner`, `lemma`, `identifier`. *Bardzo przydatny do testowania*.
- solvers.null_solver - solver, który zwraca dokładnie taki wynik jaki otrzymał. Posiada metody def `lemma`, `identifier`.
*Przydatny do testowania* (gdy np. robimy moduł ner i nie potrzebujemy lemmatizera i identifiera).
- solvers.polyglot - solver oparty na bibliotece polyglot. Z nieznanych przyczyn czasem wywala się na języku rosyjskim. Nie ma danych dla języka bułgarskiego. Posiada metodę: `ner`
- solvers.composite - solver do łączenia wyników z różnych solver'ów. Ze względu na uniwersalność, może być użyty jako każdy z 3 komponentów. Posiada klasy: `And`, `Or`, `Filter`. Może być użyty np. `Or(Filter(And(solver1.ner, solver2.ner), category='PER'), solver3.ner)`
- solvers.prych - solver obsługujący lematyzację na podstawie pliku od dr Pawła Rychlikowskiego. Ma klasę: `Lemma` 
- solvers.type0 - solver bazujący na rozwiązaniu type0 - dla każdego wejścia przyporządkowuje odpowiedź, którą widział najczęściej na wyjściu. Ma metody: `ner`, `lemma`, `identifier`.
- solvers.wikidata - pierwsza implementacja  rozwiązania bazującego na danych z wikdata. Odradzam użycie - wersja 2 jest dużo lepsza.
- solver.wikidata2 - implementacja rozwiązania bazującego na wikidata. *Zużywa dużo RAMU*. Posiada metodę  `get_id_by_label_for` do przyporządkowywania międzynarodowego ID (`identifier`).

## Solvery - dodanie własnego

Solver powinien mieć dokładnie taki sam format wejścia jak i wyjścia, aby móc go połączyć z innym solverem.

![alt text](docs/images/simple_solver.png "Schema")
  
Sygnaturę interesującego solvera można podejrzeć w pliku `solver/default.py`. W przypadku gdy moduł lemmatizer lub identifier 
nie zna poprawnej odpowiedzi powinien ustawić `None`. W ten sposób umożliwi przetwarzanie tego elementu następnemu modułowi
lub moduł domyślny (będący zawsze na końcu - `Simple lemmatizer module`, `Simple identifier module`) wygeneruje domyślną wartość.
Uwaga! Po module ner jest uruchamiana heurysytyka usuwająca pod-frazy (`common.py:28` metoda `solve` 
linia `shs.default_ner..`) - np. 
wystąpienia [(79,83) - "Adam", (79, 90) - "Adam Mickiewicz"] zostaną zastąpione przez [(79, 90) - "Adam Mickiewicz"] 
i będą miały tag zewnętrznej frazy. Dodatkowo nie można zakładać nic o kolejności wykonania metod - być może w przyszłości
ze względów wydajnościowych testy będą grupowane. Wywołania powinny być niezależne od siebie.
Dodatkowo identifiery nie powinny być łączone wewnętrze tylko za pomocą odpowiednich zagnieżdżen composite solver.
Nie powinny usuwać żadnego nera z listy otrzymanej jako parametr (chyba, że mają ku temu jakiś powód).
from solvers.utils import LemmatizerType
Zalecany workflow:

1. Napisanie funkcji w notebook.ipynb realizującej naszą funkcjonalność
2. Uruchomienie jej i optymalizacja
3. Jeśli funkcja zużywa zbyt dużo pamięci, można użyć dekoratora `@profile` z `
from memory_profiler import profile` 
4. Po jej stworzeniu dodajemy ją do odpowiedniego (nowego lub istniejącego) pliku w katalogu `solvers/`
5. Warto zanotować gdzieś wynik danego modułu, gdyż do konkursu można wysłać więcej niż jedną odpowiedź

Przykładowa implementacja klasy pełniącej rolę lemmatizera:

```python3
from solvers.utils import LemmatizerType

class Lemma:
    def __init__(self):
        #prepare before running, i.e. load data
        pass

    def __call__(self, lang, text, ners: LemmatizerType) -> LemmatizerType:
        #do something with ners
        for index, (phrase, lemma, typpe) in enumerate(ners):
            if lemma == "None" and lang == "pl":
                lemma = "jakis-lemat"
            ners[index] = (phrase, lemma, typpe)
        return ners

```
