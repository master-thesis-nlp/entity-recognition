import functools
import time
from collections import defaultdict
from functools import lru_cache
from multiprocessing.pool import ThreadPool

import gc

from data_provider import get_separated_data_for_language
from metrics import category_composite
from metrics import mentions_catch
from tqdm import tqdm

import solvers.default as sol
import solvers.heuristics.default as shs
from solvers import type0
from solvers.utils import generate_temporary_id


def timeit(func):
    @functools.wraps(func)
    def newfunc(*args, **kwargs):
        startTime = time.time()
        x = func(*args, **kwargs)
        elapsedTime = time.time() - startTime
        elapsed_s = int(elapsedTime)
        elapsed_ms = int((elapsedTime - elapsed_s) * 1000)
        elapsed_min = int(elapsed_s/60)
        elapsed_s = elapsed_s % 60

        print('function [{}] finished in {} min {} s {} ms'.format(
            func.__name__, elapsed_min, elapsed_s, elapsed_ms))
        return x

    return newfunc


class WorkerTask:
    def __init__(self, lang, ner, lemmatizer, identifier, function):
        self.lang = lang
        self.ner = ner
        self.lemmatizer = lemmatizer
        self.identifier = identifier
        self.function = function

    def __call__(self, text):
        return self.function(text, self.lang, self.ner, self.lemmatizer, self.identifier)


def get_accuracy(tests, lang, ner=sol.ner, lemmatizer=sol.lemmatizer, identifier=sol.identifier,
                 answer_fixer=lambda lang, x: x, crosslang_answer_fixer=lambda x: x):
    """
    :returns [(entity, type, lemma, id)]
    """
    answers = []
    exps = []
    for _, text, exp in tests:
        # answers += [text]
        answers += [solve(text, lang, ner, lemmatizer, identifier)]
        exps += [exp]
    # with ThreadPool() as pool:
    #     t = WorkerTask(lang, ner, lemmatizer, identifier, solve)
    #     # answers = pool.map(t, answers)
    #     answers = list(map(t, answers))
    answer_fixer(lang, answers)
    return zip(exps, answers)


def solve(text, lang, ner=sol.ner, lemmatizer=sol.lemmatizer, identifier=sol.identifier):
    ners = ner(lang, text, [])
    ners = list(set(ners))
    ners = shs.default_ner(ners)

    # converting from ner format to lemmatizer format
    ners = [(indices, None, typpe) for indices, typpe in ners]

    ners = lemmatizer(lang, text, ners)

    # converting from lemmatizer format to identifier format
    if len(ners) > 0 and len(ners[0]) != 3:
        print("Got wrong length of ner entity: ", ners[0])
        print("Expected 3 elements")
    for ind, (indices, lemma, typpe) in enumerate(ners):
        word = indices
        if type(indices) == tuple:
            b, e = indices
            word = text[b:e]
        if lemma is None:
            lemma = word
        ners[ind] = (word, lemma, typpe, None)

    ners = identifier(lang, text, ners)

    # removing None identifier with default generated
    for ind, (indices, lemma, typpe, idd) in enumerate(ners):
        if idd is None:
            idd = generate_temporary_id(typpe, lemma)
        ners[ind] = (indices, lemma, typpe, idd)

    return ners


def run_on_data(train, test, lang, **method):
    type0.train_data = train
    type0.train_lang = lang
    answers = get_accuracy(test, lang, **method)
    return answers


def execute_for_language(test_provider, lang, **method):
    answers = []
    for train, test in test_provider(lang):
        answers += run_on_data(train, test, lang, **method)
    return answers


def compare_methods(
        metric_factory=lambda: mentions_catch.MentionsCatch(),
        langs=None,
        categories=None,
        test_provider=get_separated_data_for_language,
        **methods):
    """
    This method is used to compare two or more modules with given metric
    :param metric_factory:
    :param categories:
    :param methods: arguments can be ner, lemmatizer, identifier, answer_fixer, crosslang_answer_fixer
    :param langs:
    :param test_provider:
    :return: res
    """
    gc.collect()
    if categories is None:
        categories = ["PER", "PRO", "ORG", "LOC", "EVT", "All"]
    if langs is None:
        langs = ['pl', 'cs', 'ru', 'bg', "All"]
    res = [("Lang", "Category", "Method", "F1", "PRECIS", "P[NUM]", "RECALL", "R[NUM]", "metric")]
    it = tqdm(methods.items()) if len(methods) > 1 else methods.items()
    exception_list = []
    for desc, method in it:
        # for lang in tqdm(langs):
        for lang in langs:
            if lang == "All":
                answers_dict = {}
                for lang_ in ["pl", "cs", "bg", "ru"]:
                    answers_dict[lang_] = execute_for_language(test_provider, lang_, **method)

                crosslang_answer_fixer = method.get("crosslang_answer_fixer", lambda x: x)
                answers_dict = crosslang_answer_fixer(answers_dict)  # lang -> [[(phrase, lemma, typpe, idd)],[]]

                metric = category_composite.CategoryComposite(metric_factory, category=categories)
                for lang_ in ["pl", "cs", "bg", "ru"]:
                    for exp, ans in answers_dict[lang_]:
                        metric.add(exp, ans)

                # for lang, answers in answers_dict.items():
                m = metric.get_precision_and_recall_and_f1()
                if type(m) == dict:
                    for cat, (prec, p_num, rec, r_num, f1) in m.items():
                        res += [(lang, cat, desc, f1, prec, p_num, rec, r_num, str(metric))]
                else:
                    p, r, f = m
                    res += [(lang, "All", desc, f, p, r, str(metric))]
            else:
                metric = category_composite.CategoryComposite(metric_factory, category=categories)
                for train, test in test_provider(lang):
                    type0.train_data = train
                    type0.train_lang = lang
                    answers = get_accuracy(test, lang, **method)
                    for exp, ans in answers:
                        metric.add(exp, ans)
                m = metric.get_precision_and_recall_and_f1()
                if type(m) == dict:
                    for cat, (prec, p_num, rec, r_num, f1) in m.items():
                        res += [(lang, cat, desc, f1, prec, p_num, rec, r_num, str(metric))]
                else:
                    p, p_num, r, r_num, f = m
                    res += [(lang, "All", desc, f, p, p_num, r, r_num, str(metric))]
        for el, m in method.items():
            print("Flush on ", el, str(type(m)))
            try:
                m.flush()
            except:
                print("Got exception ")
                pass
    if exception_list:
        print("Got exceptions [", len(exception_list), "]")
        print(exception_list)
    return res


def show_mistakes_made_by_method(lang, method, metric_factory, num, category_str,
                                 offset=0,
                                 fail_check=lambda p, r: (p != 1.0 and p is not None) or (r != 1.0 and r is not None),
                                 data_provider=get_separated_data_for_language):
    for train, test in data_provider(lang):
        type0.train_data = train
        type0.train_lang = lang
        for data in train:
            metric = metric_factory()
            answers = get_accuracy([data], lang, **method)
            for exp, ans in answers:
                metric.add(exp, ans)
            #         print(len(metric.ans), " vs ", len(metric.exp))
            metric.ans = list(filter(lambda x: x[2] == category_str, metric.ans))
            metric.exp = list(filter(lambda x: x[2] == category_str, metric.exp))
            #         print(len(metric.ans), " vs ", len(metric.exp))
            p, p_num, r, r_num, f = metric.get_precision_and_recall_and_f1()
            if fail_check(p, r):
                if offset == 0:
                    print(p, r, f)
                    ans = defaultdict(lambda: [])
                    for p, l, t, i in metric.ans:
                        ans[i] += [(p, l, t)]
                    exp = defaultdict(lambda: [])
                    for p, l, t, i in metric.exp:
                        exp[i] += [(p, l, t)]
                    print("Ans: ")
                    for key, v in ans.items():
                        print(key, v)
                    print()
                    print("Exp: ")
                    for key, v in exp.items():
                        print(key, v)
                    num -= 1
                    if num == 0:
                        break
                    else:
                        print()
                        print()
                else:
                    offset -= 1


@timeit
def compare_identifier_methods(
        ner,
        lemma,
        cached=True,
        metric_factory=lambda: mentions_catch.MentionsCatch(),
        langs=None,
        categories=None,
        test_provider=get_separated_data_for_language,
        **methods):
    m = {}
    if cached:
        ner = CacheAndSkipSecondPass(ner)
        lemma = CacheAndSkipSecondPass(lemma)
    for name, conf in methods.items():
        m[name] = {"ner": ner, "lemmatizer": lemma, **conf}
    res = compare_methods(
        metric_factory=metric_factory,
        langs=langs,
        categories=categories,
        test_provider=test_provider,
        **m
    )
    if cached:
        print("NER cached: ", ner.ratio())
        print("Lemma cached: ", lemma.ratio())
    return res


class CacheAndSkipSecondPass:
    def __init__(self, target):
        self.target = target
        self.cache = {}
        self.total = 0
        self.hit = 0

    def ratio(self):
        return str(self.hit) + "/" + str(self.total)

    def __call__(self, lang: str, text: str, l: list):
        h = (lang, text)
        self.total += 1
        if h not in self.cache or self.cache[h][1] != l:
            self.cache[h] = (self.target(lang, text, l), list(l))
        else:
            self.hit += 1
        return list(self.cache[h][0])
