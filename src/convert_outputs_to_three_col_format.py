import glob
import sys

if len(sys.argv) < 3:
    exit("Nie podano wejścia i wyjścia")


raw_inp_pref = sys.argv[1]
raw_out_pref = sys.argv[2]
print("input prefix ", raw_inp_pref)
print("output prefix ", raw_out_pref)
loc = raw_inp_pref + "/*/*.out"
file_names = glob.glob(loc)
for file in file_names:
    parts = file.split("/")
    file_name = parts[-1]
    lang = parts[-2]
    print(file, "->", raw_out_pref + lang + "/" + file_name)
    output_file = open(raw_out_pref + lang + "/" + file_name, "w+", encoding="UTF8")
    for line in open(file, "r", encoding="UTF8"):
        if line.count("\t") == 3:
            tags = line.split("\t")
            output_file.write("\t".join(tags[0:3]))
            output_file.write("\n")
        else:
            output_file.write(line)
