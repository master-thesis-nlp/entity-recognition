import glob
from functools import lru_cache

import numpy
import math


def get_train_and_test_for_language(lang='pl', train_percent=0.8, iters=1):
    """Returns tuple (train, test)
       where both have same format = [(id, texts, annotation)]
        annotation = [(Named-entity-mention, base-form, category, cross-lingual-ID)]"""
    assert (0 < train_percent < 1)
    data = get_data_for_language(lang)
    for i in range(iters):
        data = numpy.random.permutation(data)
        train, test = numpy.split(data, [math.floor(train_percent * len(data))])
        yield train, test


def get_train_and_test_k_fold(lang='pl', k=5):
    assert (0 < k < 10)
    data = get_data_for_language(lang)
    data = numpy.random.permutation(data)
    l = int(len(data) / k) * k
    data = data[0:l]
    data = data.reshape((5, -1, 4))
    for i in range(k):
        test = data[i]
        train = []
        for j in range(len(data)):
            if i != j:
                train += list(data[i])
        yield train, test


def get_all_data_as_test(lang):
    raw, annotated = get_file_list_for_language(lang)
    phrase = "asia_bibi"
    files_test = zip(raw, annotated)
    test = []
    for file_raw, file_annotated in files_test:
        data = get_data_from_file(file_raw, file_annotated, lang)
        test += [data]
    train = []
    yield train, test


def get_all_data_as_train(lang):
    for train, test in get_all_data_as_test(lang):
        yield test, train


class BSNLPBlindTest:
    def __init__(self, types: list = ["ryanair", "nord_stream"], langs: list = ["pl", "ru", "bg", "cs"],
                 limit_data=None,
                 test_data_loc="",
                 training_data_provider=lambda lang: []):
        self.types = types
        self.langs = langs
        self.training_data_provider = training_data_provider
        self.data = {}
        for lang in self.langs:
            res = []
            for t in self.types:
                raw_pref = test_data_loc + "application_data/test_data_from_bsnlp/raw/" + t + "/" + lang + "/"
                raw_path = raw_pref + "*.txt"
                raw_file_names = [raw_pref + f.split("/")[-1] for f in glob.glob(raw_path)]

                ann_pref = test_data_loc + "application_data/test_data_from_bsnlp/annotated/" + t + "/" + lang + "/"
                ann_path = ann_pref + "*.answer"
                ann_file_names = [ann_pref + f.split("/")[-1] for f in glob.glob(ann_path)]

                for file_raw, file_annotated in zip(sorted(raw_file_names), sorted(ann_file_names)):
                    #                     print(file_raw, file_annotated)
                    data = get_data_from_file(file_raw, file_annotated, lang)
                    res += [data]
            if limit_data and limit_data < len(res):
                self.data[lang] = res[0:limit_data]
            else:
                self.data[lang] = res
            assert(len(self.data[lang]) > 0)

    def __call__(self, lang):
        if lang not in self.langs:
            raise Exception("You haven't loaded " + lang + " into bsnlp. Loaded only " + str(self.langs))
        if lang in self.data:
            yield self.training_data_provider(lang), self.data[lang]
        else:
            yield self.training_data_provider(lang), []


def get_separated_data_for_language(lang):
    """Return asia_bibi as test and brexit as training"""
    # yield train, test
    yield get_brexit_for_lang(lang), get_asia_bibi_for_lang(lang)


def get_reversed_separated_data_for_language(lang):
    """Return brexit as test and asia_bibi as training"""
    # yield train, test
    yield get_asia_bibi_for_lang(lang), get_brexit_for_lang(lang)


@lru_cache(maxsize=None)
def get_brexit_for_lang(lang):
    raw, annotated = get_file_list_for_language(lang)
    phrase = "brexit"
    files_test = zip(list(filter(lambda x: phrase in x, raw)), list(filter(lambda x: phrase in x, annotated)))
    test = []
    for file_raw, file_annotated in files_test:
        data = get_data_from_file(file_raw, file_annotated, lang)
        test += [data]
    return test


@lru_cache(maxsize=None)
def get_asia_bibi_for_lang(lang):
    raw, annotated = get_file_list_for_language(lang)
    phrase = "asia_bibi"
    files_test = zip(list(filter(lambda x: phrase in x, raw)), list(filter(lambda x: phrase in x, annotated)))
    test = []
    for file_raw, file_annotated in files_test:
        data = get_data_from_file(file_raw, file_annotated, lang)
        test += [data]
    return test


@lru_cache(maxsize=None)
def get_data_for_language(lang='pl'):
    """Returns tuple (id, texts, annotation)
        annotation = [(Named-entity-mention, base-form, category, cross-lingual-ID)]"""
    files_raw, files_annotation = get_file_list_for_language(lang)
    res = []
    for file_raw, file_annotated in zip(files_raw, files_annotation):
        data = get_data_from_file(file_raw, file_annotated, lang)
        res += [data]
    return res


@lru_cache(maxsize=None)
def get_data_from_file(file_raw, file_annotated, lang):
    with open(file_annotated) as f:
        lines = list(map(lambda x: x.strip(),
                         f))
        ans = []
        for a in list(map(lambda x: tuple(
                map(lambda x: x.strip(),
                    x.split('\t'))), lines))[1:]:
            if len(a) != 4:
                print("Skipping answer: ", a, " as it doesnt contain 4 elements")
            else:
                ans += [a]
        with open(file_raw) as r:
            lines = list(map(lambda x: x.strip(),
                             r))
            # print(lines)
            assert (lines[1] == lang)
            text = lines[4] + ". " + " ".join(list(filter(lambda x: len(x) > 0, lines[5:])))
            return lines[0], text, ans


@lru_cache(maxsize=None)
def get_file_list_for_language(lang):
    raw_pref = "application_data/training_data/raw/" + lang + "/"
    ann_pref = "application_data/training_data/annotated/" + lang + "/"
    loc = raw_pref + "/*.txt"
    file_names = [f.split("/")[-1] for f in glob.glob(loc)]
    file_names = [".".join(f.split('.')[0:2]) for f in file_names]

    if len(file_names) == 0:
        print("Couldn't find files for lang=", lang, " in location ", loc)
        exit(3)

    files_raw = []
    for f in file_names:
        file_name = raw_pref + f + ".txt"
        if lang == "pl":
            file_name = file_name.replace(".txt.txt", ".txt")
        files_raw += [file_name]
    files_annotation = []
    for f in file_names:
        file_name = ann_pref + f + ".out"
        if lang == "pl" and "txt.out" in file_name:
            file_name = file_name.replace("txt.", "")
        files_annotation += [file_name]
    return files_raw, files_annotation
