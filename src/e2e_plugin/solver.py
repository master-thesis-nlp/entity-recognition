import pickle
import time
from collections import defaultdict

import requests
import json

import tqdm
from googletrans import Translator
from mezmorize import Cache
from google.cloud import translate_v2 as translatev2

from metrics import mentions_catch, category_composite
import six

cache = Cache(CACHE_TYPE='filesystem', CACHE_DIR='translate_cache')
translator = Translator()

total_calls = 0
total_miss = 0
translate_dict = {}


def translate(text, source_lang, target_lang="en"):
    global total_calls, translate_dict, total_miss
    total_calls += 1
    if total_calls == 1:
        try:
            with open("application_data/translate_api.pickle", "rb") as file:
                translate_dict = pickle.load(file)
        except FileNotFoundError:
            print("Couldn't load cache")
            pass
    if total_calls % 10 == 0:
        with open("application_data/translate_api.pickle", "wb") as file:
            pickle.dump(translate_dict, file)
        # skip
    idd = source_lang + "#" + text
    if idd not in translate_dict:
        total_miss += 1
        try:
            translate_dict[idd] = translator.translate(text, dest=target_lang, src=source_lang).text
            time.sleep(1)
        except Exception:
            # print("Can't use free translation, switching to paid")
            translate_dict[idd] = translate_paid(text, source_lang)
            # print("Translated ", idd, " to ", translate_dict[idd])
            time.sleep(1)
    return translate_dict[idd]


def translate_paid(text, source_lang):
    print("translate paid ", text, source_lang)
    if isinstance(text, six.binary_type):
        text = text.decode('utf-8')
    translate_client = translatev2.Client()
    result = translate_client.translate(
        text, source_language=source_lang, target_language="en")

    # print(u'Text: {}'.format(result['input']))
    # print(u'Translation: {}'.format(result['translatedText']))
    # print(u'Detected source language: {}'.format(
    #     result['detectedSourceLanguage']))
    return result['translatedText']


class E2EPlugin:
    def __init__(self, translate=False, verbose=False):
        self.gerbil_port = "5555"
        self.translate = translate
        self.verbose = verbose
        print("E2e plugin verbose=", self.verbose)

    def request_gerbil(self, t: str):
        data = {"text": t, "spans": []}
        r = requests.post('http://localhost:' + self.gerbil_port, data=json.dumps(data))
        return json.loads(r.content)

    def gerbil_answer(self, ans: list, t: str):
        r = self.request_gerbil(t)
        res = []
        for (s, e, idd) in r:
            cat = "EVT"
            phrase = t[s:s + e]
            for mention, _, category, _ in ans:
                if mention == phrase:
                    cat = category
                    break
            res += [(phrase, phrase, cat, idd)]
        return res

    def translateAnswer(self, ans, lang):
        res = []
        for (phrase, lemma, category, idd) in ans:
            if lemma == phrase:
                phrase = translate(phrase, lang)
                lemma = phrase
            else:
                phrase = translate(phrase, lang)
                lemma = translate(lemma, lang)
            res += [(phrase, lemma, category, idd)]
        return res

    def getAnswerForTrain(self, langs: list, data_provider, metric_factory=lambda: mentions_catch.MentionsCatch(),
                          categories=None):
        return self.getAnswer(langs, data_provider, metric_factory, categories, data_selector=lambda train, test: train)

    def getAnswerForTest(self, langs: list, data_provider, metric_factory=lambda: mentions_catch.MentionsCatch(),
                         categories=None):
        return self.getAnswer(langs, data_provider, metric_factory, categories, data_selector=lambda train, test: test)

    def getAnswer(self, langs: list, data_provider, metric_factory=lambda: mentions_catch.MentionsCatch(),
                  categories=None, data_selector=lambda x, y: []):
        res = []
        if categories is None:
            categories = ["PER", "PRO", "ORG", "LOC", "EVT", "All"]
        for lang in langs:
            metric = category_composite.CategoryComposite(metric_factory, category=categories)
            for train, test in data_provider(lang):
                data = data_selector(train, test)
                for t_id, text, ans in tqdm.tqdm(data, desc="lang=" + lang):
                    text_eng = text
                    if self.translate:
                        text_eng = translate(text, lang)
                        ans = self.translateAnswer(ans, lang)
                        if self.verbose:
                            print("---Input text after translation:\n", text_eng)
                            print("---Answers after translation:")
                            for ph, l, t, i in ans:
                                print(ph, "&", l, "&", t, "&", i, "\\")
                            print("---------")
                    g_ans = self.gerbil_answer(ans, text_eng)
                    if self.verbose:
                        print("---Gerbil answer:")
                        for ph, l, t, i in g_ans:
                            print(ph, "&", l, "&", t, "&", i, "\\")
                        print("---------")
                    metric.add(ans, g_ans)
            m = metric.get_precision_and_recall_and_f1()
            if type(m) == dict:
                for cat, (prec, p_num, rec, r_num, f1) in m.items():
                    if self.verbose:
                        print("---Score [", lang, cat, "]:\n", f1, prec, p_num, rec, r_num, str(metric))
                    res += [(lang, cat, "E2E_Method", f1, prec, p_num, rec, r_num, str(metric))]
            else:
                p, p_num, r, r_num, f = m
                if self.verbose:
                    print("---Score [", lang, "]:\n", f, p, p_num, r, r_num, str(metric))
                res += [(lang, "All", "E2E_Method", f, p, p_num, r, r_num, str(metric))]
        return res
        # self.calculateAnswer()


class E2ESolver:
    def __init__(self, plugin=None, lemma=None, verbose=False):
        if plugin is None:
            plugin = E2EPlugin(translate=True, verbose=verbose)
        if lemma is None:
            lemma = lemmaNone
        self.solver = plugin
        self.lemma = lemma
        self.stats = defaultdict(lambda: 0)
        self.verbose = verbose

    def solve(self, lang, text):
        if self.verbose:
            print("---------Next text---")
            print("---Input:\n", text)
            print("---------")
        ners = self.getNers(lang, text)
        text = text.lower()
        lemma_text = self.lemma(text)
        if self.verbose:
            print("---Lemmatized text:")
            print(lemma_text)
            print("-------")
        res = []
        if self.verbose:
            print("---Answers with lemmas")
        for word, idd in ners:
            self.stats["total_phrase"] += 1
            proposed_lemma = self.lemma(word).lower()
            if self.verbose:
                print(word, "&", proposed_lemma, "&", idd, "\\\\")
            if word in text:
                res += [(word.strip(";)(][.,:\"'-"), proposed_lemma, "None", idd)]
            elif proposed_lemma in lemma_text:
                w = proposed_lemma.split(" ")
                ls = lemma_text.split(" ")
                for i in range(0, lemma_text.count(" ") + 2 - len(w)):
                    if ls[i:i + len(w)] == w:
                        res += [
                            (" ".join(text.split(" ")[i:i + len(w)]).strip(";)(][.,:\"'-"), proposed_lemma, "None",
                             idd)]
                        break
            else:
                self.stats["missed_phrase"] += 1
                print(word)
        if self.verbose:
            print("--------")
            print("--- E2E anwers filtered:")
            for p, l, _, i in res:
                print(p, "&", l, "&", i, "\\\\")
            print("---------")
        return res

    def assignCategories(self, res, exp):
        r = []
        for phrase, lemma, typpe, idd in res:
            self.stats["total_cat_ass"] += 1
            for p, l, t, _ in exp:
                if p.lower() == phrase.lower() or lemma.lower() == l.lower():
                    typpe = t
                    self.stats["matched_cat_ass"] += 1
                    break
            if typpe == "None":
                typpe = "PER"
                print("Got nothing for ", phrase, lemma)
            r += [(phrase, lemma, typpe, idd)]
        return r

    def getNers(self, lang, text):
        translated_text = translate(text, lang, "en")
        if self.verbose:
            print("---Translated input text:")
            print(translated_text)
        ans = self.solver.request_gerbil(translated_text)
        res = []
        if self.verbose:
            print("---E2E Answers")
        for (s, e, idd) in ans:
            if self.verbose:
                print(translated_text[s:s + e], "&", idd, "\\\\")
            phrase = translate(translated_text[s:s + e].strip(";)(][.,:\"'-"), "en", lang)
            res += [(phrase.lower(), idd)]
        if self.verbose:
            print("--------------")
            print("---E2E Answers translated:")
            for ph, i in res:
                print(ph, "&", i, "\\")
            print("---------")
        return res

    def getAnswerForTrain(self, langs: list, data_provider, metric_factory=lambda: mentions_catch.MentionsCatch(),
                          categories=None):
        return self.getAnswer(langs, data_provider, metric_factory, categories, data_selector=lambda train, test: train)

    def getAnswerForTest(self, langs: list, data_provider, metric_factory=lambda: mentions_catch.MentionsCatch(),
                         categories=None):
        return self.getAnswer(langs, data_provider, metric_factory, categories, data_selector=lambda train, test: test)

    def getAnswer(self, langs: list, data_provider, metric_factory=lambda: mentions_catch.MentionsCatch(),
                  categories=None, data_selector=lambda x, y: []):
        res = []
        if categories is None:
            categories = ["PER", "PRO", "ORG", "LOC", "EVT", "All"]
        for lang in langs:
            metric = category_composite.CategoryComposite(metric_factory, category=categories)
            for train, test in data_provider(lang):
                data = data_selector(train, test)
                for t_id, text, ans in tqdm.tqdm(data, desc="lang=" + lang):
                    ans = [(p.lower(), l.lower(), t, i) for p, l, t, i in ans]
                    if self.verbose:
                        print("-----Correct answer", len(ans), ":")
                        for p, l, t, i in ans:
                            print(p, "&", l, "&", t, "&", i, "\\\\")
                        print("--------")
                    s = self.solve(lang, text)
                    s2 = self.assignCategories(s, ans)
                    if self.verbose:
                        print("---Answers with assigned categories:")
                        for p, l, t, i in s2:
                            print(p, "&", l, "&", t, "&", i, "\\\\")
                        print("--------")
                    metric.add(ans, s2)
            m = metric.get_precision_and_recall_and_f1()
            if type(m) == dict:
                for cat, (prec, p_num, rec, r_num, f1) in m.items():
                    res += [(lang, cat, "E2E_Method", f1, prec, p_num, rec, r_num, str(metric))]
            else:
                p, p_num, r, r_num, f = m
                res += [(lang, "All", "E2E_Method", f, p, p_num, r, r_num, str(metric))]
        return res


def lemmaNone(text):
    return text


import morfeusz2

morf = morfeusz2.Morfeusz(expand_dag=False)


def lemmaMorfeus(text):
    global morf
    r = []
    for w in text.split(" "):
        l = morf.analyse(w)[0][2][1]
        if ":" in l:
            l = l[0:l.index(":")]
        r += [l]
    return " ".join(r)
    # lemmatizer = toygger.Lemmatizer()
    # tokens = lemmatizer.lemmatize('Gdzie byłeś wczoraj wieczorem, kolego?'.split())
    # for token in tokens:
    # print(f'{token.token} {token.lemma} {token.pos}')
