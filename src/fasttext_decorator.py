import io
import os
import pickle

from memory_profiler import profile
from tqdm import tqdm
import numpy as np
import array
import time
from mezmorize import Cache
from solvers.utils import OwnTrieDict


def sizeof_fmt(num, suffix='B'):
    for unit in ['', 'Ki', 'Mi', 'Gi', 'Ti', 'Pi', 'Ei', 'Zi']:
        if abs(num) < 1024.0:
            return "%3.1f%s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f%s%s" % (num, 'Yi', suffix)


# 481 mb, 25s
def save_transformation(vect, file_path):
    with io.open(file_path, 'w', encoding="utf-8", newline="\n") as file:
        file.write(str(len(vect)) + " -1\n")
        for key, item in tqdm(vect.items(), desc="writing transformation result to file " + file_path):
            file.write(key + " " + " ".join(map(str, item)))


# 94 mb, 0.4s
def fast_save_bin_transformation(vect, file_path):
    start = time.time()
    if not os.path.exists("application_data/fasttext_transformed"):
        os.mkdir("application_data/fasttext_transformed")
    pickle.dump(vect, open(file_path, "wb"), protocol=pickle.HIGHEST_PROTOCOL)
    end = time.time()
    print("Save done in: ", end - start, "s")


@profile
def fast_read_bin_transformation(file_path):
    with open(file_path, "rb") as file:
        res = pickle.load(file)
    return res


def load_vectors(fname):
    with io.open(fname, 'r', encoding='utf-8', newline='\n', errors='ignore') as fin:
        n, d = map(int, fin.readline().split())
        data = {}
        x = 0
        for line in tqdm(fin, total=2000000, desc="reading from file"):
            tokens = line.rstrip().split(' ')
            repr2 = array.array('f', map(float, tokens[1:]))
            data[tokens[0]] = repr2
            # if x > 80000:
            #    break
            x += 1
        return data


def uniformSpace(fname, dict):
    with io.open(fname, 'r', encoding='utf-8', newline='\n', errors='ignore') as fin:
        matrix = []
        for line in fin:
            l = line.split(' ')
            matrix += [array.array('f', map(float, l))]
        matrix = np.array(matrix)
        for key in tqdm(dict, desc="transforming"):
            dict[key] = array.array('f', matrix.dot(dict[key]))
        return dict


def calculate_transformation(file_in, transformation, file_out):
    vect = load_vectors(file_in)
    vect = OwnTrieDict(uniformSpace(transformation, vect))
    # save_transformation(vect, file_out)
    fast_save_bin_transformation(vect, file_out)
    return vect


def load_languages(langs=['pl']):
    res = []
    for lang in langs:
        striped_file = 'application_data/fasttext_stripped/strip.cc.' + lang + '.300.vec.pickle'
        try:
            with open(striped_file, "rb") as file:
                vect = pickle.load(file)
                print("Using stripped file")
        except (ValueError, FileNotFoundError, EOFError):
            print("Stripped file not found in path: ", striped_file)
            cached_file = 'application_data/fasttext_transformed/transf.cc.' + lang + '.300.vec.pickle'
            try:
                print("Loading vect for lang = ", lang, flush=True)
                # vect = load_vectors(cached_file)
                vect = fast_read_bin_transformation(cached_file)
                print("done")
            except (ValueError, FileNotFoundError, EOFError):
                print("Recalculating transformation for lang = ", lang, flush=True)
                vect = calculate_transformation('application_data/fasttext_original/cc.' + lang + '.300.vec',
                                                'application_data/external_libs/fastText_multilingual/alignment_matrices/' + lang + '.txt',
                                                cached_file)
        res += [vect]
    return res
