import glob
import itertools


class GeneralDataProvider:
    def __init__(self, testsets: list, lang: list, rel_datapath: str = ""):
        self.loaded_langs = lang
        self.blind_provider = BlindTestDataProvider(rel_datapath + "application_data/test_data_from_bsnlp/")
        self.data_provider = {"ryanair": lambda l: self.blind_provider.get_test(l, "ryanair")}
        for t in testsets:
            assert (t in self.get_available_testsets())
        self.testsets = testsets

    def get_available_testsets(self) -> list:
        return list(self.data_provider.keys())

    def get_data_for_lang(self, lang) -> list:
        assert (lang in self.loaded_langs)
        for t in self.testsets:
            for x in self.data_provider[t](lang):
                yield x

    def __call__(self, lang) -> list:
        yield [], [x for x in self.get_data_for_lang(lang)]  # don't have answer?


class BlindTestDataProvider:
    def __init__(self, testloc: str):
        self.testloc = testloc

    def get_test(self, lang: str, test: str):
        annotated_pattern = self.testloc + "annotated/{}/{}/*.txt"
        annotated_pattern = annotated_pattern.format(test, lang)
        annotated = [x for x in glob.glob(annotated_pattern)]

        raw_pattern = self.testloc + "raw/{}/{}/*.txt"
        raw_pattern = raw_pattern.format(test, lang)
        raw = [x for x in glob.glob(raw_pattern)]
        for r, a in zip(raw, annotated):
            yield get_data_from_file(r, a, lang)


def get_data_from_file(file_raw, file_annotated, lang):
    with open(file_annotated) as f:
        lines = list(map(lambda x: x.strip(),
                         f))
        ans = []
        for a in list(map(lambda x: tuple(
                map(lambda x: x.strip(),
                    x.split('\t'))), lines))[1:]:
            if len(a) != 4:
                print("Skipping answer: ", a, " as it doesnt contain 4 elements")
            else:
                ans += [a]
        with open(file_raw) as r:
            lines = list(map(lambda x: x.strip(),
                             r))
            # print(lines)
            assert (lines[1] == lang)
            text = lines[4] + ". " + " ".join(list(filter(lambda x: len(x) > 0, lines[5:])))
            return lines[0], text, ans
