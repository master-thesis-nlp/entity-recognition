import glob
import sys
from collections import defaultdict

from tqdm import tqdm

from solvers import default
from solvers.fixers import simplefixer
from solvers.utils import generate_temporary_id
from solvers.wikidatan import identifier
from solvers.wikidatan import repository

if len(sys.argv) == 2:
    exit("Nie podano folderu z wejściami")


def add_identifiers(lang, data, answer_fixer, identifier=default.identifier):
    ans = []
    for ners in tqdm(data, desc="iteration_over_files"):
        for ind, (indices, lemma, typpe) in enumerate(ners):
            ners[ind] = (indices, lemma, typpe, None)
        ners = identifier(lang, "", ners)
        # removing None identifier with default generated
        for ind, (indices, lemma, typpe, idd) in enumerate(ners):
            if idd is None:
                idd = generate_temporary_id(typpe, lemma)
            ners[ind] = (indices, lemma, typpe, idd)
        ans += [ners]
    answer_fixer(lang, ans)
    return ans


gl_rep = repository.Repository(preload_dictionaries=False)
configuration = {"identifier": identifier.Identifier(gl_rep),
                 "answer_fixer": simplefixer.SimpleFixer(gl_rep, 0.9, 0.9, categories=["ORG"], enrich=False)
                 # "answer_fixer": lambda l, x: x
                 }

raw_inp_pref = sys.argv[1]
raw_out_pref = sys.argv[2]
print("input prefix ", raw_inp_pref)
print("output prefix ", raw_out_pref)
loc = raw_inp_pref + "/*/*.with_lemmas"
# print(loc)
file_names = glob.glob(loc)

# print(file_names)
data = defaultdict(list)
for file in file_names:
    parts = file.split("/")
    file_name = parts[-1]
    lang = parts[-2]
    with open(file, "r", encoding="UTF8") as inp_stream:
        idd = inp_stream.readline().strip()
        assert(idd.count("\t") == 0)
        ners = []
        for line in inp_stream:
            line = line.strip()
            els = line.split("\t")
            if line.count("\t") != 3:
                print(file, "line: ", line)
                assert(line.count("\t") == 3)
            ners += [tuple([els[0].lower(), els[1].lower(), els[2]])]
        data[lang] += [(file_name, idd, ners)]
for lang, d in tqdm(data.items(), total=len(data)):
    res = add_identifiers(lang, [ners for file_name, idd, ners in data[lang]], **configuration)
    for ind, (x, (file_name, idd, ners)) in enumerate(zip(res, data[lang])):
        data[lang][ind] = (file_name, idd, x)
    for file_name, idd, ners in d:
        file = raw_out_pref + lang + "/" + file_name + ".with_ids"
        print("Writing for ", file_name, idd, lang)
        print("to  file ", file)
        with open(file, "w+", encoding="UTF8") as out_stream:
            out_stream.write(idd + "\n")
            for tup in ners:
                out_stream.write("\t".join(tup) + "\n")


