class CategoryComposite:
    def __init__(self, metric_factory, category=None):
        if category is None:
            category = ["PER", "PRO", "ORG", "LOC", "EVT", "All"]
        self.metrics = {cat:metric_factory() for cat in category}

    def __str__(self):
        return str(list(self.metrics.values())[0])

    def add(self, expected, answer):
        for cat, item in self.metrics.items():
            self.metrics[cat].add(filter_to_category(expected, cat), filter_to_category(answer, cat))

    def get_precision_and_recall_and_f1(self):
        return {cat:item.get_precision_and_recall_and_f1() for cat, item in self.metrics.items()}

def filter_to_category(ans, category):
    answer = [(entity, lemma, typpe, idd) for entity, lemma, typpe, idd in ans if
              typpe == category or category == "All"]
    return answer
