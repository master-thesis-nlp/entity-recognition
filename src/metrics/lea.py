# based on: http://www.aclweb.org/anthology/P16-1060
from collections import Counter, defaultdict
from fractions import Fraction
from itertools import product

from multiset import Multiset

from solvers.prophecy import timeit


class Lea:
    def __init__(self, remove_repetition=False, exact=True):
        self.exp = []
        self.ans = []
        self.remove_repetition = remove_repetition
        self.exact = exact
        pass

    def __str__(self):
        return "Lea"

    def add(self, expected, answer):
        self.exp += [(phrase.lower(), lemma.lower(), typpe.lower(), idd) for phrase, lemma, typpe, idd in expected]
        self.ans += [(phrase.lower(), lemma.lower(), typpe.lower(), idd) for phrase, lemma, typpe, idd in answer]

    def get_precision_and_recall(self):
        if self.remove_repetition:
            self.exp = set(self.exp)
            self.ans = set(self.ans)
        recall = get_metric(self.exp, self.ans, True)
        r_num = len(self.ans)
        precision = get_metric(self.ans, self.exp, self.exact)
        p_num = len(self.exp)
        return precision, p_num, recall, r_num

    def get_precision_and_recall_and_f1(self):
        p, p_num, r, r_num = self.get_precision_and_recall()
        if p is not None and r is not None and p + r > 0:
            f1 = 2 * p * r / (p + r)
        else:
            f1 = None
        return p, p_num, r, r_num, f1


def get_metric(expected, answer, exact: bool = True):
    e_links = generate_links(expected, exact)
    e_identifiers = Counter([idd for entity, lemma, typpe, idd in expected])

    e_links_by_id = defaultdict(list)
    for p1, p2, idd2 in e_links:
        e_links_by_id[idd2] += [(p1, p2)]

    a_links = Multiset(remove_id_from_links(generate_links(answer, exact)))
    up = Fraction(0)
    down = len(expected)
    for idd, cnt in e_identifiers.items():
        links_for_idd = e_links_by_id[idd]
        links_for_idd = Multiset(links_for_idd)
        common_links = len(links_for_idd.intersection(a_links))
        if len(links_for_idd) != 0:
            up += cnt * Fraction(common_links, len(links_for_idd))
    if down == 0:
        return None
    else:
        return float(up / down)


def remove_id_from_links(links):
    return [(p1, p2) for p1, p2, idd in links]


def generate_links(ners, exact=True):
    d = {}
    for entity, lemma, typpe, idd in ners:
        if idd not in d:
            d[idd] = []
        d[idd] += [entity]
    res = []
    for key, l in d.items():
        pairs = product(l, l)
        pairs = [(p1, p2, key) for p1, p2 in pairs if p1 < p2]
        if len(pairs) == 0 and exact:
            pairs = list(product([l[0]], [l[0]], [key]))
        res += pairs
    return res
