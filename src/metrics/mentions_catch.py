import numpy as np


class MentionsCatch:
    def __init__(self, lemma=False):
        self.lemma = lemma
        self.precision = []
        self.p_num = []
        self.recall = []
        self.r_num = []

    def __str__(self):
        return "lemma_catch" if self.lemma else "ner_catch"

    def add(self, expected, answer):
        if self.lemma:
            expected = [(entity, lemma) for entity, lemma, typpe, idd in expected]
            answer = [(entity, lemma) for entity, lemma, typpe, idd in answer]
        else:
            expected = [(entity, typpe) for entity, lemma, typpe, idd in expected]
            answer = [(entity, typpe) for entity, lemma, typpe, idd in answer]
        precision, p_num, recall, r_num = alg_prec_rec(expected, answer)
        self.precision += [precision]
        self.p_num += [p_num]
        self.recall += [recall]
        self.r_num += [r_num]

    def get_precision_and_recall(self):
        return (self.transform_to_results(self.precision), self.transform_to_results(self.p_num),
                self.transform_to_results(self.recall), self.transform_to_results(self.r_num))

    def get_precision_and_recall_and_f1(self):
        prec, p_num, rec, r_num = self.get_precision_and_recall()
        return prec, p_num, rec, r_num, self.get_f1(prec, rec)

    def get_f1(self, p, r):
        if p is not None and r is not None and p + r > 0:
            return 2 * r * p / (r + p)
            # f1 = 2 / (1/r + 1/p)
        else:
            return None

    @staticmethod
    def transform_to_results(arr):
        return float(np.mean(list(filter(lambda x: x is not None, arr))))


def alg_prec_rec(expected, answer):
    expected = set([(x.lower(), y.lower()) for x, y in expected])
    answer = set([(x.lower(), y.lower()) for x, y in answer])
    # print("Missing ", (set(y)-set(x)))
    # print("Got", (set(x) - set(y)))
    common_part = len(expected.intersection(answer))
    recall = common_part / len(expected) if len(expected) > 0 else None
    precision = common_part / len(answer) if len(answer) > 0 else None
    return precision, len(answer), recall, len(expected)


def alg_prec_rec_f1(expected, answer):
    p, p_num, r, r_num = alg_prec_rec(expected, answer)
    if p is not None and r is not None and p + r > 0:
        f1 = 2 * r * p / (r + p)
    else:
        f1 = None
    return p, p_num, r, r_num, f1
