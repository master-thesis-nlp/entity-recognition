from polyglot.text import Text
import numpy as np


# def getNERs(text, lang='pl'):
#     text = Text(text, hint_language_code=lang)
#     res = []
#     for sent in text.sentences:
#         for entity in sent.entities:
#             text = " ".join(sent.words[entity.start: entity.end])
#             res += [(text, text, entity.tag)]
#     return res


def diff(v1, v2):
    return v1.dot(v2) / np.linalg.norm(v1) / np.linalg.norm(v2)


def findClosestTo(vect, space, num=5):
    vect = np.array(vect)
    l = list(map(lambda kv: (diff(vect, space[kv]), kv), space))
    return sorted(l, reverse=True)[:num]
