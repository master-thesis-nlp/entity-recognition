from solvers.heuristics.ners import add_new_or_same_sub_ners


class Pipe:
    def __init__(self, solvers):
        self.solvers = solvers

    def __call__(self, lang, text, *input):
        res = input
        for solver in self.solvers:
            res = solver(lang, text, res)
        return res


class Filter:
    def __init__(self, solver, category="All", lang="Any"):
        self.solver = solver
        self.category = category
        self.lang = lang

    def __call__(self, lang, text, *args):
        if lang == self.lang or self.lang == "Any":
            res = self.solver(lang, text, *args)
        else:
            res = []
        if self.category != "All":
            res = [(phrase, tag) for phrase, tag in res if tag == self.category]
        return res


class Or:
    def __init__(self, *args, add_heuristic=add_new_or_same_sub_ners):
        self.solvers = args
        self.add_heuristic = add_heuristic

    def __call__(self, *args):
        res = []
        for solver in self.solvers:
            print(*args)
            res2 = solver(*args)
            res = self.add_heuristic(res, res2)
        return list(set(res))


class And:
    def __init__(self, *args):
        self.solvers = args

    def __call__(self, *args):
        res = set()
        for solver in self.solvers:
            res = set(solver(*args)).intersection(res)
        return list(res)
