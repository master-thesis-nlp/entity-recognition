from typing import List, Tuple

from solvers import type0, composite, polyglot, prych
from solvers.utils import IdentifierType, LemmatizerType, NerType


def identifier(lang: str, text: str, ners: IdentifierType) -> IdentifierType:
    return type0.identifier(lang, text, ners)
    # for processor in [type0.identifier]:
    #     res = processor(lang, text, ners)
    #     if res is not None:
    #         return res
    # for phrase, lemma, typpe, idd in res:
    #
    # new_id = "ID-" + lemma.upper()
    # for s in [" ", ".", ",", ":", ";", "\\", "/", "*",  "!", "?"]:
    #     new_id = new_id.replace(s, "-")
    # return new_id


def ner(lang: str, text: str, ners: NerType) -> NerType:
    # if lang in ['pl', 'cs']: # Install polyglot libs
    #     solver = composite.Or(
    #         type0.ner,
    #         polyglot.ner)
    # else:
    solver = type0.ner
    res = solver(lang, text, ners)
    return res


# prych_lemma = prych.Lemma()


def lemmatizer(lang: str, text: str, ners: LemmatizerType) -> LemmatizerType:
    """
    :param ners:
    :param lang: language of text like 'pl','bg','cs','ru'
    :param text: Whole text
    :return: lemma for word
    """
    # global prych_lemma
    # return type0.lemma(lang, text, prych_lemma(lang, text, ners))
    return None
