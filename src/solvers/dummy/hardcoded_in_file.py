import glob


class HardCodedInFile:
    def __init__(self, test_data, answer_file_masks: list, cast=lambda x: x):
        """
        :param test_data:
        :param answer_file_mask: first parameter for format is always lang,
        """
        self.test_data = test_data
        self.answer_file_masks = answer_file_masks
        self.answers = {}
        for lang in ["pl", "cs", "ru", "bg"]:
            self.answers[lang] = {}
            for answer_file_mask in answer_file_masks:
                ans_files = answer_file_mask % lang
                file_names = [f for f in glob.glob(ans_files)]
                if len(file_names) == 0:
                    print("ERROR!! Couldn't find any files for path: ", ans_files)
                    raise Exception("ERROR!! Couldn't find any files for path: " + str(ans_files))
                for f in file_names:
                    with open(f) as file:
                        idd = file.readline().strip()
                        tmp = []
                        for line in file:
                            splitted = line.strip().split("\t")
                            if len(splitted) > 1:
                                tmp += [cast(splitted)]
                            else:
                                print("Couldn't split by tabs. Got error parsing file ", f, " line ", line)
                        self.answers[lang][idd] = tmp

    def __call__(self, lang, text, ners):
        idd = self.get_idd_for_text(lang, text)
        res = []
        if lang not in self.answers:
            raise Exception("Couldn't found answers for lang=", lang, " keys: ", self.answers.keys())
        elif idd not in self.answers[lang]:
            # print("Data id for lang=" , lang, " is ", [x[0] for x in self.test_data[lang]])
            print("[" + str(
                self.answer_file_masks) + "]Couldn't found answers for lang=" + lang + " and id=" + idd + " keys: " + str(
                self.answers[lang].keys()))
        else:
            res = self.answers[lang][idd]
        return list(res)

    def get_idd_for_text(self, lang, text):
        for idd, t, _ in self.test_data[lang]:
            if t.lower() == text.lower():
                return idd
        raise Exception("Not found ner data in test set!! lang = " + lang + " text = " + text)
