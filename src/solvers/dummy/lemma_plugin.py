def find_best_match(text, phrase):
    if phrase.lower() not in text.lower():
        return None
    pos = text.lower().index(phrase.lower())
    return pos, pos + len(phrase)


class LemmaPlugin:
    def __init__(self, solver):
        self.solver = solver

    def __call__(self, lang, text, ners):
        res = self.solver(lang, text, ners)
        res2 = []
        for ind, (phrase, lemma, typpe) in enumerate(res):
            if type(phrase) == str:
                match = find_best_match(text.lower(), phrase.lower())
                if match is not None:
                    res2 += [(match, lemma, typpe)]
                else:
                    print("Couldn't find position in text of phrase", phrase)
            else:
                res2 += [res[ind]]
            if typpe not in ["PRO", "PER", "ORG", "LOC", "EVT"]:
                raise Exception("Received wrong type ", typpe, " instead of one of 5 expected")
        return res2
