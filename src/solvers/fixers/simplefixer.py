import gc
import pickle
from itertools import product

import numpy as np
from tqdm import tqdm

from solvers.utils import IdentifierType


def vector_similarity(langs, phrase1: str, phrase2: str):
    v1 = phrase_to_vect(langs, phrase1)
    v2 = phrase_to_vect(langs, phrase2)
    return vector_on_vect_similarity(v1, v2)


def phrase_to_vect(fasttext, phrase1: str):
    v1 = np.zeros(300)
    for word in phrase1.lower().split(" "):
        if word in fasttext:
            v1 += fasttext[word]
    v1 = v1 / np.linalg.norm(v1)
    return v1


def phrase_to_vect_with_stat(fasttext, phrase1: str, lang: str, typpe: str):
    v1 = np.zeros(300)
    hit = 0
    total = 0
    for word in phrase1.lower().split(" "):
        if word in fasttext:
            v1 += fasttext[word]
            hit += 1
        total += 1
    v1 = v1 / np.linalg.norm(v1)
    return v1, total, hit


def vector_on_vect_similarity(v1, v2):
    return sum(v1 * v2)


from collections import defaultdict


class FindAndUnion:
    def __init__(self, keys, sort, pr_repo):
        self.keys = keys
        if sort:
            keys = sorted(keys, key=lambda k: pr_repo.get_pagerank_for(k), reverse=True)
        self.key_map = {k: k for k in keys}

    def find(self, idd):
        if self.key_map[idd] != idd:
            self.key_map[idd] = self.find(self.key_map[idd])
            return self.key_map[idd]
        else:
            return idd

    def union(self, idd, idd2):
        idd = self.find(idd)
        idd2 = self.find(idd2)
        if idd != idd2:
            self.key_map[idd2] = idd


class MulitLangWrapper:
    def __init__(self, fixer):
        self.fixer = fixer

    def __call__(self, answers) -> IdentifierType:
        id2vect = defaultdict(list)
        # print(answers.items())
        for lang, tests in answers.items():
            for exp, answer in tests:
                for phrase, lemma, typpe, idd in answer:
                    if typpe in self.fixer.categories:
                        id2vect[idd] += [phrase_to_vect(self.fixer.get_fasttext(lang), lemma)]
        print("Multilang: ")
        fu = self.fixer.generate_mapping(id2vect)
        print("Multilang end")
        for lang, tests in answers.items():
            for exp, answer in tests:
                for ind2, (phrase, lemma, typpe, idd) in enumerate(answer):
                    if typpe in self.fixer.categories:
                        answer[ind2] = (phrase, lemma, typpe, fu.find(idd))
        return answers


class SeparateFixer:
    def __init__(self, repo, conf: list):
        self.fixers = defaultdict(dict)
        self.statistics = defaultdict(lambda: defaultdict(lambda: 0))
        for x in conf:
            fx = SimpleFixer(repo, x["th"], x["th"], categories=[x["cat"]], enrich=False)
            self.fixers[x["lang"]][x["cat"]] = fx
            fx.statistics = self.statistics

    def reload_words(self):
        for lang, cat_fixers in self.fixers.items():
            for cat, f in cat_fixers.items():
                f.reload_words()

    def __call__(self, lang, answers) -> IdentifierType:
        if lang not in self.fixers:
            print("Skipped fixing for lang=", lang, " -> available langs, ", self.fixers.keys())
            return answers
        if lang in self.fixers:
            for cat, f in self.fixers[lang].items():
                print("[SeparateFixer] start processing ", lang, cat)
                answers = f(lang, answers)
                print("[SeparateFixer] end processing ", lang, cat)
        return answers


class SimpleFixer:
    def __init__(self, repo, f1, f2, enrich=True, sort=True, categories=None):
        if categories is None:
            categories = ["ORG", "PER", "EVT", "LOC", "PRO"]
        self.f1 = f1
        self.f2 = f2
        assert (self.f2 >= self.f1)
        self.repo = repo
        # training parameters
        self.enrich = enrich
        self.sort = sort
        self.categories = categories
        self.fasttext_cached = (None, None)
        self.statistics = defaultdict(lambda: defaultdict(lambda: 0))
        self.words = {"pl": set(), "ru": set(), "bg": set(), "cs": set()}

    def get_fasttext(self, lang):
        if self.fasttext_cached[0] != lang:
            self.fasttext_cached = (lang, None)  # clean up resources before use
            gc.collect()
            self.fasttext_cached = (lang, self.repo.get_fasttext_for_lang(lang))
        return self.fasttext_cached[1]

    def reload_words(self):
        with open("application_data/fasttext_stripped", "rb") as file:
            r = pickle.load(file)
            print("Loaded ", list(map(lambda x: len(x), r.values())))
            for k,v in r.items():
                self.words[k] = self.words[k].union(v)
        with open("application_data/fasttext_stripped", "rb") as file:
            print("Loaded ", list(map(lambda x: len(x), r.values())))
            pickle.dump(r, file)

    def generate_mapping(self, id2vect) -> FindAndUnion:
        stat_pref = self.fasttext_cached[0]  # lang
        fu = FindAndUnion(list(id2vect.keys()), self.sort, self.repo)
        self.statistics[stat_pref]["total_keys"] += len(id2vect.keys())
        for ind, key in tqdm(enumerate(fu.keys), total=len(fu.keys), desc="simple fixer"):
            q_id = 1 if key[0] == "Q" else 0
            for key2 in fu.keys[ind + 1:]:
                if fu.find(key) != fu.find(key2):
                    local_q_id = 1 if key2[0] == "Q" else 0
                    factor = self.f1 if q_id + local_q_id == 1 else self.f2
                    for vect2, vect in product(id2vect[key], id2vect[key2]):
                        similarity = vector_on_vect_similarity(vect2,
                                                               vect)  # need to propagate information about language
                        self.statistics[stat_pref]["sum_similarity"] += similarity
                        self.statistics[stat_pref]["num_similiarity"] += 1
                        if similarity > factor:
                            self.statistics[stat_pref]["total_merges"] += 1
                            fu.union(key, key2)
                            break
        return fu

    def __call__(self, lang, answers) -> IdentifierType:
        if len(answers) == 0:
            print("Skipping fixing as answers has 0 length")
            return answers
        id2vect = defaultdict(list)
        for ans in answers:
            for phrase, lemma, typpe, idd in ans:
                if typpe in self.categories:
                    for w in lemma.split(" "):
                        self.words[lang].add(w)
                    vect, total, hit = phrase_to_vect_with_stat(self.get_fasttext(lang), lemma, lang, typpe)
                    id2vect[idd] += [vect]
                    stat_pref = lang + "_" + typpe
                    self.statistics[stat_pref]["fasttext_hit"] += hit
                    self.statistics[stat_pref]["fasttext_total_words"] += total
                # enrich_propositions
                # if self.enrich: # commented out as it is not used now
                #     new_prop = []
                #     ids = set([i for _, i in phrase_prop])
                #     for idd2 in ids:
                #         new_prop += [(l, idd2) for l in self.repo.get_names_for_id(idd2, typpe)]
                #     phrase_prop += new_prop
        fu = self.generate_mapping(id2vect)
        # answer transformation
        for x, ans in enumerate(answers):
            for y, (phrase, lemma, typpe, idd) in enumerate(ans):
                if typpe in self.categories:
                    stat_pref = lang + "_" + typpe
                    self.statistics[stat_pref]["total_mentions"] += 1
                    self.statistics[stat_pref]["change_mentions"] += 1 if fu.find(idd) != idd else 0
                    self.statistics[stat_pref]["sum_words"] += lemma.count(" ") + 1
                    answers[x][y] = (phrase, lemma, typpe, fu.find(idd))
        #     print(len(id2ans))
        #     raise Exception
        return answers
