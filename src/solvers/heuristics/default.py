from solvers.heuristics.ners import remove_subsequences


def default_ner(res):
    res = remove_subsequences(res, 'All')
    return res
