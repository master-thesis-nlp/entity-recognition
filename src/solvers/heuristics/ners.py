import numpy as np


def remove_subsequences(inp, category):
    res2 = list(filter(lambda x: x[1] != category and category != "All", inp))
    inp = list(filter(lambda x: x[1] == category or category == "All", inp))
    inp2 = sorted(inp, key=lambda x: -np.linalg.norm(x[0][1] - x[0][0]))
    res = []
    for (s1, e1), cat in inp2:
        found = False
        for (s2, e2), cat2 in res:
            if cat2 == cat:
                if s1 <= e2 and e1 >= s2:
                    found = True
                    break

        if not found:
            res += [((s1, e1), cat)]
    return res + res2


def add_new_or_same_sub_ners(old, new):
    res = old.copy()
    for x in new:
        phrase, type = x
        found = False
        for phrase2, type2 in old:
            if (range_overlapping(range(phrase2[0], phrase2[1]), range(phrase[0], phrase[1]))
                    and type2 != type):
                found = True
        if not found:
            res += [x]
    return res


def range_overlapping(x, y):
    if x.start == x.stop or y.start == y.stop:
        return False
    return ((x.start < y.stop and x.stop > y.start) or
            (x.stop > y.start and y.stop > x.start))
