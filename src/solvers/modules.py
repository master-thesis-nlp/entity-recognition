from collections import defaultdict


class Pipe:
    def __init__(self, pipe):
        self.pipe = pipe

    def __call__(self, lang, ners):
        for p in self.pipe:
            ners = p(lang, ners)
        return ners


class LocalPhraseBased:
    def __init__(self, phrase_with_external_dict_policy, phrase_alias_policy=lambda l: l,
                 phrase_in_phrase_policy=lambda l, a, b: a in b):
        self.get_aliases_for_phrase = phrase_alias_policy
        self.phrase_in_phrase_policy = phrase_in_phrase_policy
        self.phrase_with_external_dict_policy = phrase_with_external_dict_policy
        self.debug = False

    def __call__(self, lang, ners):
        # Split ners by number of words
        keys = defaultdict(lambda: [])
        catched_phrases = []
        for ind, (phrase, lemma, typpe, idd) in enumerate(ners):
            if idd is None:
                keys[lemma.count(" ") + 1] += [ind]
            else:
                catched_phrases += self.get_aliases_for_phrase([(lemma, idd)])
        for n_words, indices in keys.items():
            keys[n_words] = sorted(indices, key=lambda ind: len(ners[ind][1]), reverse=True)
        # Iterate over it by number of words decresing
        for n_words, indices in sorted(keys.items(), reverse=True):
            if self.debug:
                print("------------------")
                print("iteration ", n_words)
                print("cached phrases: ", catched_phrases)
            # Add catched phrases
            for ind, (entity, lemma, typpe, idd_) in ((ind, ners[ind]) for ind in indices if ners[ind][3] is None):
                for phrase, idd in reversed(catched_phrases):
                    if self.phrase_in_phrase_policy(lang, lemma, phrase):
                        catched_phrases += self.get_aliases_for_phrase([(lemma, idd)])
                        ners[ind] = (entity, lemma, typpe, idd)
                        break
                    elif self.phrase_in_phrase_policy(lang, entity, phrase):
                        catched_phrases += self.get_aliases_for_phrase([(entity, idd)])
                        ners[ind] = (entity, lemma, typpe, idd)
                        break
            if self.debug:
                print("part 2 iteration ", n_words)
                print("cached phrases: ", catched_phrases)
            # Iterate over word with length n_words
            for ind, (entity, lemma, typpe, idd_) in ((ind, ners[ind]) for ind in indices if ners[ind][3] is None):
                context_phrase = False
                for phrase, idd in reversed(catched_phrases):
                    if self.phrase_in_phrase_policy(lang, lemma, phrase):
                        catched_phrases += self.get_aliases_for_phrase([(lemma, idd)])
                        ners[ind] = (entity, lemma, typpe, idd)
                        context_phrase = True
                        break
                    elif self.phrase_in_phrase_policy(lang, entity, phrase):
                        catched_phrases += self.get_aliases_for_phrase([(entity, idd)])
                        ners[ind] = (entity, lemma, typpe, idd)
                        context_phrase = True
                        break
                if not context_phrase:
                    res = self.phrase_with_external_dict_policy(lang, entity, lemma)
                    if res:
                        catched_phrases += self.get_aliases_for_phrase(res)
                        idd_ = res[0][1]
                        ners[ind] = (entity, lemma, typpe, idd_)
            if self.debug:
                print("end of iteration ", n_words)
                print("cached phrases: ", catched_phrases)
            catched_phrases = list(set(catched_phrases))
        return ners
