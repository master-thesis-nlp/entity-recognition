from solvers.utils import LemmatizerType, IdentifierType


def lemma(lang, text, ners:LemmatizerType) -> LemmatizerType:
    return ners


def identifier(lang, text, ners: IdentifierType) -> IdentifierType:
    return [(phrase, lemma, typpe, idd) for phrase, lemma, typpe, idd in ners]