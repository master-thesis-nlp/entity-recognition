from polyglot.text import Text

from solvers.utils import NerType


#TODO test
def ner(lang, text, ners:NerType) -> NerType:
    ner_tag_translation = {"I-LOC": "LOC", "I-ORG": "ORG", "I-PER": "PER"}
    text = Text(text, hint_language_code=lang)
    res = []
    for sent in text.sentences:
        for entity in sent.entities:
            start = text.index(sent.words[entity.start], sent.start, sent.end)
            end = text.index(sent.words[entity.end-1], start, sent.end)
            res += [((start, end + len(sent.words[entity.end-1])), ner_tag_translation[entity.tag])]
    return ners + res
