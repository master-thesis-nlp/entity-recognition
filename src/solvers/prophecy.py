import functools
import time
from collections import defaultdict

from common import timeit
from data_provider import get_data_for_language
from solvers.utils import fast_substring_indexes, NerType, LemmatizerType, IdentifierType
import numpy as np

lang_dict = {}
lemma_as_word = False


def ner(l, text, ners: NerType, types='All') -> NerType:
    lang_data = load_data_for_language(l)
    for id, t, annotation in lang_data[text[0:6]]:
        if t == text:
            res = []
            for ann in annotation:
                # print(ann)
                occurances = list(fast_substring_indexes(ann[0].lower(), text.lower()))
                # if len(occurances) == 0:
                # print("Not found ", ann[0], " in ", text)
                # raise Exception("Not found ", ann[0], " in ", text)
                for ind in occurances:
                    try:
                        res += [((ind, ind + len(ann[0])), ann[2])]
                    except:
                        # pass
                        raise Exception("Not found ", ann[0], " in ", text)
            return res
    raise Exception("Not found ner data in test set!! Got " + str(
        len(lang_data)) + " elements loaded; lang = " + l + " text = " + text)


def lemma(l, text, ners: LemmatizerType) -> LemmatizerType:
    lang_data = load_data_for_language(l)
    ch = []
    for ind, (id, t, annotation) in enumerate(lang_data[text[0:6]]):
        if t == text:
            ch += [ind]
    assert len(ch) > 0
    ch = np.random.choice(ch)
    _, _, annotation = lang_data[text[0:6]][ch]
    res = []
    for phrase, lemma, typpe, idd in annotation:
        try:
            if lemma_as_word:
                b = text.lower().index(phrase.lower())
                e = b + len(phrase)
                indices = (b, e)
                res += [(indices, lemma, typpe)]
            else:
                res += [(phrase, lemma, typpe)]
        except:
            pass
    return res


def identifier(l, text, _: IdentifierType) -> IdentifierType:
    lang_data = load_data_for_language(l)
    for id, t, annotation in lang_data[0:6]:
        if t == text:
            return annotation
    assert False, "Shoudln't be here - no identifier found for data"


def load_data_for_language(l) -> dict:
    global lang_dict
    if l not in lang_dict:
        lang_miss(l)
    return lang_dict[l]


@timeit
def lang_miss(l):
    print("Loading lang ", l, " for oracle")
    print("Keys: ", lang_dict.keys())
    d = get_data_for_language(l)
    lang_dict[l] = defaultdict(list)
    for id, t, annotation in d:
        lang_dict[l][t[0:6]] += [(id, t, annotation)]
