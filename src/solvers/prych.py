from solvers.utils import LemmatizerType


class Lemma:
    def __init__(self):
        self.mapper = {}
        with open("application_data/prych/wiki_lemmatization.txt") as file:
            for line in file:
                word, lemma = line.split("---")
                self.mapper[word.strip()] = lemma.strip()

    def __call__(self, lang, text, ners: LemmatizerType) -> LemmatizerType:
        if lang == "pl":
            for ind, (indices, lemma, typpe) in enumerate(ners):
                b, e = indices
                if lemma is None and text[b:e] in self.mapper:
                    ners[ind] = (indices, self.mapper[text[b:e]], typpe)
        return ners
