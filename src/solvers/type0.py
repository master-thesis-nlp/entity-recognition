import errno
import pickle
import os
import numpy as np
from collections import Counter

from solvers.modules import Pipe, LocalPhraseBased
from solvers.utils import fast_substring_indexes, generate_temporary_id, NerType, LemmatizerType, IdentifierType

remembered_ner_words = (None, None)
train_data = None
train_lang = None


def ner(lang, text, ners: NerType) -> NerType:
    return ner_as_positions(lang, text)


def ner_as_positions(lang, text) -> NerType:
    global remembered_ner_words
    if remembered_ner_words[0] != lang:
        load_lang(lang)
    res = []
    for entity, cat in remembered_ner_words[1]:
        try:
            pos = (m for m in fast_substring_indexes(entity, text))
            # pos = [text.index(entity)]
            res += [((p, p + len(entity)), cat) for p in pos]
        except ValueError:
            pass
    return list(set(res))


def lemma(lang, text, ners: LemmatizerType) -> LemmatizerType:
    res = []
    for indices, lemma, typpe in ners:
        if lemma is None:
            b, e = indices
            word = text[b:e]
            global remembered_ner_words
            if remembered_ner_words[0] != lang:
                load_lang(lang)
            prop = []
            for entity, l in remembered_ner_words[2]:
                if entity == word:
                    prop += [l]
            if len(prop) > 0:
                lemma = np.random.choice(prop)
        res += [(indices, lemma, typpe)]
    return res


def identifier(lang, text, ners: IdentifierType) -> IdentifierType:
    load_lang(lang)
    global remembered_ner_words
    if remembered_ner_words[0] != lang:
        load_lang(lang)
    res = []
    for phrase, lemma, typpe, idd in ners:
        if idd is None:
            idd = _find_id_for_word(lemma)
        res += [(phrase, lemma, typpe, idd)]
    return res


def _find_id_for_entity(phrase, lemma):
    r = _find_id_for_word(lemma)
    return (lemma, r) if r else (phrase, _find_id_for_word(phrase))


def _find_id_for_word(word) -> str:
    idds = []
    global remembered_ner_words
    for entity, idd in remembered_ner_words[3]:
        if entity == word:
            idds += [idd]
    return np.random.choice(idds) if len(idds) > 0 else None


def identifier_with_localphrase(lang, text, ners: IdentifierType) -> IdentifierType:
    load_lang(lang)
    p = LocalPhraseBased(
        phrase_with_external_dict_policy=lambda lang, phrase, lemma: [_find_id_for_entity(phrase, lemma)])
    return p(lang, ners)


def generate_ner_lang(lang):
    """
    Returns (lang, [(phrase, lemma, typpe, idd)..])
    """
    assert (lang in ['pl', 'cs', 'ru', 'bg'])
    res = []
    # x = get_data_for_language(lang)
    global train_data
    assert (train_lang == lang), "You forgot to properly set language in training_data"
    assert (train_data is not None), "Train data not set. Did you forget to set type0.train_data?"
    for _, _, annotated in train_data:  # get_data_for_language(lang):
        res += annotated
    ner_data = assign_most_common([(clear_ner_phrase(phrase), typpe) for phrase, lemma, typpe, idd in res],
                                  "type lang=" + lang)
    lemma_data = assign_most_common([(clear_ner_phrase(phrase), lemma) for phrase, lemma, typpe, idd in res],
                                    "lemma lang=" + lang)
    identifiers = assign_most_common([(lemma, idd) for phrase, lemma, typpe, idd in res], "id lang=" + lang)

    return lang, ner_data, lemma_data, identifiers


def clear_ner_phrase(phrase):
    return phrase.strip(" .,:;")


def assign_most_common(data, desc=''):
    """
    :returns list of pair that first element is unique, and second is the most frequent
    """
    res = {}
    for entity, _ in Counter(data).most_common():  # , desc="generating cache for " + desc):
        if entity[0] not in res:
            res[entity[0]] = entity[1]
    return list(res.items())


def load_lang(lang, random_sed=-1):
    global remembered_ner_words
    remembered_ner_words = generate_ner_lang(lang)


def load_lang_deprecated(lang):  # might used as cache, but needs sed
    file_name = "./application_data/caches/type0/" + lang + '.ners.cache.pickle'
    if not os.path.exists(os.path.dirname(file_name)):
        try:
            os.makedirs(os.path.dirname(file_name))
        except OSError as exc:  # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise
    try:

        return pickle.load(open(file_name, 'rb'))
    except FileNotFoundError as e:
        res = generate_ner_lang(lang)
        with open(file_name, 'wb+') as handle:
            pickle.dump(res, handle, protocol=pickle.HIGHEST_PROTOCOL)
        return load_lang_deprecated(lang)
