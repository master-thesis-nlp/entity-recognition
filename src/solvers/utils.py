from typing import List, Tuple, NewType, Any

import numpy as np

import marisa_trie
from tqdm import tqdm

IdentifierType = NewType('IdentifierType', List[Tuple[str, str, str, Any]])
LemmatizerType = NewType('LemmatizerType', List[Tuple[str, Any, str]])
NerType = NewType('NerType', List[Tuple[Tuple[int, int], str]])


def fast_substring_indexes(substring: str, string: str):
    last_found = -1  # Begin at -1 so the next position to search from is 0
    while True:
        # Find next index of substring, by starting after its last known position
        last_found = string.find(substring, last_found + 1)
        if last_found == -1:
            break  # All occurrences have been found
        yield last_found


def generate_temporary_id(pref: str, word: str) -> str:
    return pref + "-" + translate_cyrillic(word.upper().replace(" ", "-"))


def translate_cyrillic(text: str) -> str:
    symbols = (u"абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ",
               u"abvgdeejzijklmnoprstufhzcss_y_euaABVGDEEJZIJKLMNOPRSTUFHZCSS_Y_EUA")
    tr = {ord(a): ord(b) for a, b in zip(*symbols)}
    text = text.translate(tr)
    for f, t in {"ks": "x", "kz": "x"}.items():
        text = text.replace(f, t)
        text = text.replace(f.upper(), t.upper())
    return text


def compress_id(idd: str) -> int:
    if type(idd) != str or idd[0] != "Q":
        print("Given wrong id = ", idd)
        print("Id passed to compress id should be string and look like 'Q12345'")
        exit(0)
    return int(idd[1:])


def decompress_id(idd: int) -> str:
    if type(idd) != int:
        print("Decompressed id should be int, not: ", idd)
    return "Q" + str(idd)


def make_shrt(w):
    res = ""
    for x in w.split(" "):
        if len(x) > 0:
            if x.isdigit():
                res += x
            else:
                res += x[0]
    return res


def make_shrt_only_capital(w):
    res = ""
    for x in w.split(" "):
        if len(x) > 0:
            if x.isdigit():
                res += x
            elif x[0].isupper():
                res += x[0]
    return res


assert (make_shrt("Unia w Europie") == "UwE")
assert (make_shrt_only_capital("Unia w Europie") == "UE")
assert (make_shrt("Polskie radio 24") == "Pr24")
assert (make_shrt("Polskie Radio 24") == "PR24")
assert (make_shrt_only_capital("Polskie radio 24") == "P24")


def levenstein_distance(w1: str, w2: str) -> int:
    arr = np.zeros((len(w1) + 1, len(w2) + 1))
    arr[:, 0] = np.linspace(0, len(w1), len(w1) + 1)
    arr[0, :] = np.linspace(0, len(w2), len(w2) + 1)
    for i, c in enumerate(w1):
        for j, c2 in enumerate(w2):
            n = 0 if c == c2 else 1
            arr[i + 1, j + 1] = min(arr[i + 1 - 1, j + 1] + 1, arr[i + 1, j + 1 - 1] + 1, arr[i, j] + n)
            if i > 0 and j > 0:
                n = 1 if w1[i] + w1[i - 1] == w2[j - 1] + w2[j] else 2
                arr[i + 1, j + 1] = min(arr[i + 1, j + 1], arr[i - 1, j - 1] + n)
    #   print(arr)
    return arr[len(w1), len(w2)]


class OwnTrieDict:
    def __init__(self, dictionary: dict):
        self.values = list(range(len(dictionary)))
        self.trie = marisa_trie.Trie(dictionary.keys())
        for k, v in tqdm(dictionary.items(), total=len(dictionary)):
            x = tuple(v) if type(v) == list else v
            self.values[self.trie[k]] = v
        # self.values = tuple(self.values)

    def __getitem__(self, item):
        # print("req ", item)
        # print("index", self.trie[item])
        return self.values[self.trie[item]]

    def __contains__(self, item):
        return item in self.trie

    def __len__(self):
        return len(self.values)

    def items(self):
        for k in self.trie.keys():
            yield k, self.values[self.trie[k]]


assert levenstein_distance("aaaa", "aaaa") == 0
assert levenstein_distance("aa", "aaaa") == 2
assert levenstein_distance("aaba", "aaab") == 1
assert levenstein_distance("abcd", "cdab") == 4
assert levenstein_distance("aaaa", "a") == 3
assert levenstein_distance("leave.eu", "leave.ue") == 1
