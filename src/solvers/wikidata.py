import pickle
import numpy as np
from difflib import SequenceMatcher

from memory_profiler import profile
from mezmorize import Cache

import pywikibot

from solvers.utils import generate_temporary_id

id_to_item = {}
human_to_id = {}
org_to_id = {}
loc_to_id = {}


# 1. identify full person name (> 1 word)
# 2. identify this persons name or surnames
# 3. identify nationalities -> TODO
# 4. identify surnames of popular person
def get_id_by_label_for(lang, text, ners):
    start_size = len(ners)
    assert False, "This function shouldn't be used"
    persons = [(entity, lemma, typpe) for entity, lemma, typpe, idd in ners if typpe == "PER"]
    organizations = [(entity, lemma, typpe) for entity, lemma, typpe, idd in ners if typpe == "ORG"]
    products = [(entity, lemma, typpe) for entity, lemma, typpe, idd in ners if typpe == "PRO"]
    locations = [(entity, lemma, typpe) for entity, lemma, typpe, idd in ners if typpe == "LOC"]
    events = [(entity, lemma, typpe) for entity, lemma, typpe, idd in ners if typpe == "EVT"]
    others = [(entity, lemma, typpe, generate_temporary_id("OTH", lemma)) for entity, lemma, typpe in ners if
              typpe not in ["PER", "ORG", "PRO", "LOC"]]

    persons = get_id_by_label_for_persons(lang, text, persons)
    organizations = get_id_by_label_for_organization(lang, text, organizations)
    products = get_id_by_label_for_products(lang, text, products)
    locations = get_id_by_label_for_locations(lang, text, locations)
    res = persons + organizations + products + locations + others
    assert len(res) == start_size
    return res


def get_id_by_label_for_products(lang, text, ners):
    return [(entity, lemma, typpe, generate_temporary_id("OTH", lemma)) for entity, lemma, typpe in ners]


def get_id_by_label_for_locations(lang, text, ners):
    locations = load_locations()
    res = []
    for entity, lemma, typpe in ners:
        idd = locations.get(lemma, None)
        if entity in locations:
            idd = locations[entity]
        res += [(entity, lemma, typpe, idd)]
    return res


def get_human_id_by_word(lang, word):
    h2i = load_human_list()  # take after date of birth
    return h2i.get(word, None)


def get_id_by_label_for_organization(lang, text, ners):
    organizations = load_organizations()
    res = []
    # shrt_to_id = {}
    for entity, lemma, typpe in ners:
        idd = None
        if lemma in organizations:
            idd = organizations[lemma]
        if entity in organizations:
            idd = organizations[entity]
        res += [(entity, lemma, typpe, idd)]
    for no, (entity, lemma, typpe, idd) in enumerate(res):
        if idd is None:
            res[no] = (entity, lemma, typpe, generate_temporary_id("ORG", lemma))
    return res


def get_id_by_label_for_persons(lang, text, ners):
    res = []
    localPersonToId = {}
    # identify full name
    for entity, lemma, typpe in ners:
        idd = None
        assert typpe == "PER"
        if " " in lemma:  # more than one word
            idd = get_human_id_by_word(lang, lemma)
            if idd is not None:
                localPersonToId[lemma] = idd
            else:
                idd = get_human_id_by_word(lang, entity)
                if idd is not None:
                    localPersonToId[entity] = idd
                elif is_human_name(lang, lemma.split(" ")[0]):
                    idd = generate_temporary_id("PER", lemma)
                    localPersonToId[entity] = idd
        res += [(entity, lemma, typpe, idd)]
    # print("KEYS:", localPersonToId.keys())
    # idenitfy mentioned person's name/surnames
    for ind, (entity, lemma, typpe, idd) in enumerate(res):
        if idd is None:
            prop = []
            for name, id_ in localPersonToId.items():
                if lemma in name:
                    prop += [id_]
            if len(prop) > 0:
                res[ind] = (entity, lemma, typpe, np.random.choice(prop))
    # identify nationalities and popular surnames (shou
    for ind, (entity, lemma, typpe, idd) in enumerate(res):
        if idd is None and lemma in human_to_id:
            idd = human_to_id[lemma]
        if idd is None and entity in human_to_id:
            idd = human_to_id[entity]
        res[ind] = (entity, lemma, typpe, idd)  # TODO make some popularity rank
    # identify popular persons surnames
    for ind, (entity, lemma, typpe, idd) in enumerate(res):
        if typpe == "PER" and idd is None:
            prop = []
            for name, id_ in human_to_id.items():
                if lemma in name.split(" ")[-1]:
                    prop += [id_]
            if len(prop) > 0:
                res[ind] = (entity, lemma, typpe, np.random.choice(prop))
    for ind, (entity, lemma, typpe, idd) in enumerate(res):
        if idd is None:
            res[ind] = (entity, lemma, typpe, generate_temporary_id("PER", lemma))
    assert len(res) == len(ners)
    return res


def is_human_name(lang, word):
    return True


def get_item(idd):
    assert (idd[0] == "Q")
    if idd in id_to_item:
        return id_to_item[idd]
    else:
        return get_item_from_server(idd)


cache = Cache(CACHE_TYPE='filesystem', CACHE_DIR='cache')


@cache.memoize()
def get_item_from_server(idd):
    print("Cache miss: ", idd)
    site = pywikibot.Site("wikidatan", "wikidatan")
    repo = site.data_repository()
    item = pywikibot.ItemPage(repo, idd)
    return item.get()


def load_all_known_repos():
    load_wikidata_repo_pickle('names')
    load_wikidata_repo_pickle('familias')


def load_organizations():
    global org_to_id
    if len(org_to_id) == 0:
        repo = pickle.load(open("application_data/wikidata/" + "organizations" + ".pickle", "rb"))
        for idd, val in repo.items():
            for key, name in val.items():
                if key.startswith("l_"):
                    org_to_id[name] = idd
            for x in val.get('short name', []):
                if x not in org_to_id:
                    org_to_id[x] = idd
    return org_to_id


def load_wikidata_repo_pickle(file_name):
    repo = pickle.load(open("application_data/wikidata/" + file_name + ".pickle", "rb"))
    global id_to_item
    id_to_item = {**id_to_item, **repo}
    for idd in repo.keys():
        id_to_item[idd]['type'] = 'CACHED-' + file_name


def load_human_list():
    global human_to_id
    global id_to_item
    if len(human_to_id) == 0:
        # load_all_known_repos()
        human_list = pickle.load(open("application_data/wikidata/humans.pickle", "rb"))
        # extraction
        for idd, item in human_list.items():
            l = []
            for interesting_prop in ["l_", "nat"]:
                for prop_name, value in item.items():
                    if value is None:
                        continue
                    elif isinstance(value, tuple) or isinstance(value, list):
                        l += list(value)
                    else:
                        l += [value]
            for word in l:
                assert (isinstance(word, str))
                human_to_id[word] = idd  # TODO popularity
        # print(human_list[0])
        id_to_item = {**id_to_item, **human_list}
        # for idd in human_list.keys():
        #     id_to_item[idd]['type'] = 'HUMAN'
    return human_to_id


def load_locations():
    global loc_to_id
    if len(loc_to_id) == 0:
        locations = pickle.load(open("application_data/wikidata/locations.pickle", "rb"))
        for idd, items in locations.items():
            for label, item in items.items():
                loc_to_id[item] = idd
    return loc_to_id
