import pickle
from collections import defaultdict
import numpy as np

from solvers.utils import generate_temporary_id, make_shrt, make_shrt_only_capital, levenstein_distance, IdentifierType


def get_id_by_label_for(lang, text, ners:IdentifierType) -> IdentifierType:
    ners_by_typpe = defaultdict(lambda: [])
    for entity, lemma, typpe, idd in ners:
        ners_by_typpe[typpe] += [(entity, lemma, typpe, idd)]
    ners = []
    for typpe, ner in ners_by_typpe.items():
        if typpe == "PER":
            ners += get_id_by_label_for_persons(lang, text, ner)
        if typpe == "ORG":
            ners += get_id_by_label_for_organizations(lang, text, ner)
        if typpe == "PRO":
            ners += get_id_by_label_for_products(lang, text, ner)
        # if typpe == "LOC":
        #     ners += get_id_by_label_for_locations(lang, text, ner)
        # elif typpe == "EVT":
        #     ners += get_id_by_label_for_events(lang, text, ner)
        else:
            # print("GOT ", typpe)
            # exit(1)
            ners += ner
    return ners


def get_id_by_label_for_products(lang, text, ners):
    ners = algorithm(lang, ners,
                     policy_of_add_identifier=products_add_id_by_wikihumandict,
                     on_policy_result=on_policy_result_add_shrt)
    ners = algorithm(lang, ners,
                     policy_of_add_identifier=lambda _, entity, lemma: tmp_id_policy("PRO", entity, lemma),
                     on_policy_result=on_policy_result_add_shrt)
    return ners


def get_id_by_label_for_locations(lang, text, ners):
    ners = algorithm(lang, ners, location_add_id_by_wikihumandict)
    ners = algorithm(lang, ners, lambda _, entity, lemma: tmp_id_policy("LOC", entity, lemma))
    return ners


def get_id_by_label_for_events(lang, text, ners):
    ners = algorithm(lang, ners, lambda _, entity, lemma: tmp_id_policy("EVT", entity, lemma))
    return ners


def get_id_by_label_for_organizations(lang, text, ners):  # typpe == ORG
    ners = algorithm(lang, ners, organization_add_id_by_wikiorgdict,
                     on_policy_result=on_policy_result_add_shrt)
    ners = algorithm(lang, ners, lambda _, entity, lemma: tmp_id_policy("ORG", entity, lemma),
                     on_policy_result=on_policy_result_add_shrt)
    return ners


def get_id_by_label_for_persons(lang, text, ners, policies=True):
    ners = algorithm(lang, ners, person_add_id_by_wikihumandict)
    if policies:
        ners = algorithm(lang, ners, lambda _, entity, lemma: tmp_id_policy("PER", entity, lemma))
    return ners


def choose_best_from_list(lang, l):
    if type(l) == tuple or type(l) == list:
        # native_and_local = list(
        #     filter(lambda x: x[2] and x[1] == lang, l))  # TODO maybe something smarter if precision goes down..
        local = list(
            filter(lambda x: x[1] == lang, l))
        # if len(native_and_local) > 0:
        #     # return native_and_local[np.random.choice(range(0, len(native_and_local)))][0]
        #     return native_and_local[0][0] # TODO deterministic most popular
        if len(local) > 0:
            return local[0][0]
            # return native[np.random.choice(range(0, len(native)))][0]
        else:
            return l[0][0]
    else:
        return l


def products_add_id_by_wikihumandict(lang: str, entity: str, lemma: str) -> list:
    word_to_id = get_word_to_id("PRO")
    if lemma in word_to_id:
        idd_ = choose_best_from_list(lang, word_to_id[lemma])
        return [(lemma, idd_)]
    if entity in word_to_id:
        idd_ = choose_best_from_list(lang, word_to_id[entity])
        return [(entity, idd_)]
    return []


def location_add_id_by_wikihumandict(lang: str, entity: str, lemma: str) -> list:
    word_to_id = get_word_to_id("LOC")
    if lemma in word_to_id:
        idd_ = choose_best_from_list(lang, word_to_id[lemma])
        return [(lemma, idd_)]
    if entity in word_to_id:
        idd_ = choose_best_from_list(lang, word_to_id[entity])
        return [(entity, idd_)]
    return []


def organization_add_id_by_wikiorgdict(lang: str, entity: str,
                                       lemma: str) -> list:  # TODO maybe add short names of organization
    word_to_id = get_word_to_id("ORG")
    if lemma in word_to_id:
        idd_ = choose_best_from_list(lang, word_to_id[lemma])
        return [(lemma, idd_)]
    if entity in word_to_id:
        idd_ = choose_best_from_list(lang, word_to_id[entity])
        return [(entity, idd_)]
    word_to_id = get_word_to_id("ORG_shrt")
    if lemma in word_to_id:
        idd_ = choose_best_from_list(lang, word_to_id[lemma])
        return [(lemma, idd_)]
    if entity in word_to_id:
        idd_ = choose_best_from_list(lang, word_to_id[entity])
        return [(entity, idd_)]
    return []


def person_add_id_by_wikihumandict(lang: str, entity: str, lemma: str) -> list:
    word_to_id = get_word_to_id("PER")
    if lemma in word_to_id:
        idd_ = choose_best_from_list(lang, word_to_id[lemma])
        return [(lemma, idd_)]
    if entity in word_to_id:
        idd_ = choose_best_from_list(lang, word_to_id[entity])
        return [(entity, idd_)]
    return []


def on_policy_result_add_shrt(ret) -> list:
    res = []
    for name, idd in ret:#TODO replace with foreach over names of idd
        res += [(name, idd)]
        shrt = make_shrt(name)
        if len(shrt) > 1:
            res += [(shrt, idd)]
        shrt = make_shrt_only_capital(name)
        if len(shrt) > 1:
            res += [(shrt, idd)]
    return res


def tmp_id_policy(pref, entity, lemma) -> list:
    idd = generate_temporary_id(pref, lemma)
    return [(lemma, idd), (entity, idd)]


def algorithm(lang, ners, policy_of_add_identifier, on_policy_result=lambda l:l, phrase_in_phrase_policy=lambda l,a,b: a in b):
    # Split ners by number of words
    keys = defaultdict(lambda: [])
    catched_phrases = []
    for ind, (phrase, lemma, typpe, idd) in enumerate(ners):
        if idd is None:
            keys[lemma.count(" ") + 1] += [ind]
        else:
            catched_phrases += on_policy_result([(lemma, idd)])
    # Iterate over it by number of words decresing
    for n_words, indices in sorted(keys.items(), reverse=True):
        # Add catched phrases
        for ind, (entity, lemma, typpe, idd_) in ((ind, ners[ind]) for ind in indices if ners[ind][3] is None):
            for phrase, idd in reversed(catched_phrases):
                ph_cnt = phrase.count(" ")
                if phrase_in_phrase_policy(lang, lemma, phrase) or phrase_in_phrase_policy(lang, entity, phrase):
                    ners[ind] = (entity, lemma, typpe, idd)
                    break
        # Iterate over word with length n_words
        for ind, (entity, lemma, typpe, idd_) in ((ind, ners[ind]) for ind in indices if ners[ind][3] is None):
            res = policy_of_add_identifier(lang, entity, lemma)
            if res:
                catched_phrases += on_policy_result(res)
                idd_ = res[0][1]
                ners[ind] = (entity, lemma, typpe, idd_)

        catched_phrases = list(set(catched_phrases))
    return ners


word_to_id = {}


def get_word_to_id(typpe) -> dict:
    langs = ['pl', 'cs', 'en', 'ru']
    if typpe not in word_to_id:
        if typpe == "PER":
            d = defaultdict(list)
            human_list = pickle.load(open("application_data/wikidata/humans.pickle", "rb"))
            for idd, value in human_list.items():
                native = value.get('nat', "")
                for l in langs:
                    label = "l_" + l
                    if label in value:
                        if type(value[label]) == tuple or type(value[label]) == list:
                            for name in value.get("l_" + l, []):
                                is_native = name in native
                                d[name] += [(idd, l, is_native)]
                        else:
                            is_native = value[label] in native
                            d[value[label]] += [(idd, l, is_native)]
            word_to_id[typpe] = d
            human_list = []
        elif typpe == "ORG":
            d = defaultdict(list)
            org_list = pickle.load(open("application_data/wikidata/organizations.pickle", "rb"))
            for idd, value in org_list.items():
                for name in value.get("official_name", []):  # TODO losing language information
                    d[name] += [(idd, None, False)]
                native = ""  # TODO dont know which name is native - losing information
                for l in langs:  # labels
                    label = "l_" + l
                    if label in value:
                        if type(value[label]) == tuple or type(value[label]) == list:
                            for name in value.get("l_" + l, []):
                                is_native = name in native
                                d[name] += [(idd, l, is_native)]
                        else:
                            is_native = value[label] in native
                            d[value[label]] += [(idd, l, is_native)]
            word_to_id[typpe] = d
        elif typpe == "ORG_shrt":
            d = []
            d2 = defaultdict(list)
            org_list = pickle.load(open("application_data/wikidata/organizations.pickle", "rb"))
            for idd, value in org_list.items():
                for l in ["official_name"]:
                    for name in value.get(l, []):  # TODO losing language information
                        d += [(name, idd, None, False)]
                for l in ["short name"]:
                    for name in value.get(l, []):  # TODO losing language information
                        d2[name] += [(idd, None, False)]
                for l in langs:  # labels
                    label = "l_" + l
                    if label in value:
                        if type(value[label]) == tuple or type(value[label]) == list:
                            for name in value.get("l_" + l, []):
                                d += [(name, idd, l, False)]
                        else:
                            d += [(value[label], idd, l, False)]
            for name, idd, l, is_native in d:
                d2[make_shrt(name)] += [(idd, l, is_native)]
                d2[make_shrt_only_capital(name)] += [(idd, l, is_native)]
            for k, v in d2.items():
                d2[k] = list(set(v))
            word_to_id[typpe] = d2
        elif typpe == "PRO":
            d = defaultdict(list)
            prod_list = pickle.load(open("application_data/wikidata/products.pickle", "rb"))
            d = add_labels_to_dict(d, prod_list)
            word_to_id[typpe] = d
        elif typpe == "LOC":
            d = defaultdict(list)
            loc_list = pickle.load(open("application_data/wikidata/locations.pickle", "rb"))
            d = add_labels_to_dict(d, loc_list)
            word_to_id[typpe] = d

    return word_to_id[typpe]


def add_labels_to_dict(d, list):
    langs = ['pl', 'cs', 'en', 'ru']
    for idd, value in list.items():
        for l in langs:  # labels
            label = "l_" + l
            if label in value:
                if type(value[label]) == tuple or type(value[label]) == list:
                    for name in value.get("l_" + l, []):
                        d[name] += [(idd, l, False)]
                else:
                    d[value[label]] += [(idd, l, False)]
    return d

def merge_similar_phrases(ners, max_dist:int, ners_compare_metric, identifier_resolve_policy):
  visited = set()
  comp = []
  for node in range(len(ners)):
    if node not in visited:
      q = set([node])
      c = []
      while len(q):
        act = q.pop()
        c += [act]
        visited.add(act)
        for neigh in range(act + 1, len(ners)):
          if neigh not in visited and levenstein_distance(ners[neigh][1], ners[act][1]) <= max_dist:
            q.add(neigh)
      comp += [c]
  for c in comp:
    if len(c) > 0:
      idd = identifier_resolve_policy([ners[x][3] for x in c])
      for ind in c:
        phrase, lemma, typpe, idd_old = ners[ind]
        ners[ind] = (phrase, lemma, typpe, idd)
  return ners

def most_common_element(lst):
  return max(set(lst), key=lst.count)
def prefer_wikidata_id(ids, conflict_policy=most_common_element): # TODO popularity
  wiki_id = list(filter(lambda x: x.startswith("Q"), ids))
  if wiki_id:
    return conflict_policy(wiki_id)
  else:
    return conflict_policy(ids)
def levenstein_ner_lemma_metric(n1, n2):
  return levenstein_distance(n1[1], n2[1])

# ---- TESTING!!!!!!!!!!!!!!!!!
def test():
    assert get_number_of_different_ids(
        get_id_by_label_for_persons("pl", {}, [('Alexandra Nixa', 'Alexander Nix', 'PER', None),
                                               ('Alexander Nix', 'Alexander Nix', 'PER', None),
                                               ('Nixa', 'Nix', 'PER', None)])) == 1
    assert get_number_of_different_ids(
        get_id_by_label_for_persons("pl", {},
                                    [('Trump', 'Trump', 'PER', None),
                                     ('Donalda Trumpa', 'Donald Trump', 'PER', None)])) == 1
    assert get_number_of_different_ids(get_id_by_label_for_persons("pl", {}, [('Trump', 'Trump', 'PER', None),
                                                                              ('Donalda Trumpa Junior',
                                                                               'Donald Trump Jr.', 'PER', None)])) == 1
    assert get_number_of_different_ids(get_id_by_label_for_persons("pl", {}, [
        ('Belki', 'Belka', 'PER', None),
        ('Marka', 'Marek', 'PER', None)])) == 2  # TODO should be one
    assert get_number_of_different_ids(
        get_id_by_label_for_persons("pl", {}, [
            ('XCSAD', 'XCSAD', 'PER', None),
            ('Marka XCSAD', 'Marek XCSAD', 'PER', None)])) == 1
    assert get_number_of_different_ids(get_id_by_label_for_organizations("pl", {}, [
        ("Unia Europejska", "Unia Europejska", "ORG", "None"),
        ("UE", "UE", "ORG", "None"),
    ])) == 1
    assert get_number_of_different_ids(get_id_by_label_for_products("pl", {}, [
        ('Sunday Timesa', 'Sunday Times', 'PRO', None), ('Sunday Times', 'Sunday Times', 'PRO', None),
        ('Times', 'Times', 'PRO', None), ('Timesa', 'Times', 'PRO', None)
    ])) == 1
    assert get_number_of_different_ids(get_id_by_label_for_products("pl", {}, [('FT', 'FT', 'PRO', None),
                                                      ('Financial Times', 'Financial Times', 'PRO', None)
                                                      ])) == 1
    assert get_number_of_different_ids(get_id_by_label_for_products("pl", {}, [
        ('białe księgi', 'Białe księgi', 'PRO', None),
        ('TVN24', 'TVN24', 'PRO', None)])) == 2

    assert get_number_of_different_ids(get_id_by_label_for_persons("ru", "", [
        ('Терезой Мэй', 'Тереза Мэй', 'PER', None),
        ('Мэй', 'Мэй', 'PER', None)
    ])) == 1

    assert get_number_of_different_ids(merge_similar_phrases([("l", "leave.eu", "ORG", "ORG-leave.eu"), ("l", "leave.ue", "ORG", "ORG-leave.ue")], 1,
                                ners_compare_metric=levenstein_ner_lemma_metric,
                                identifier_resolve_policy=prefer_wikidata_id)) == 1
    assert get_number_of_different_ids(merge_similar_phrases([("l", "leave.eu", "ORG", "ORG-leave.eu"), ("l", "leave.ue", "ORG", "Q12")], 1,
                                ners_compare_metric=levenstein_ner_lemma_metric,
                                identifier_resolve_policy=prefer_wikidata_id)) == 1
    assert get_number_of_different_ids(merge_similar_phrases([("l", "Theresa May", "ORG", "Q23"), ("l", "Teresa May", "ORG", "Q12")], 1,
                                ners_compare_metric=levenstein_ner_lemma_metric,
                                identifier_resolve_policy=prefer_wikidata_id)) == 1
    assert get_number_of_different_ids(get_pers(
        "ru", "", [
            #         ('Борис Джонсон', 'Борис Джонсон', 'PER', None),
            #         ('Бориса Джонсона', 'Борис Джонсон', 'PER', None),
            #         ('Дэвид Дэвис', 'Дэвид Дэвис', 'PER', None),
            ('Мэй', 'Мэй', 'PER', None),
            ('Терезой Мэй', 'Тереза Мэй', 'PER', None),
            ('Терезы Мэй', 'Тереза Мэй', 'PER', None)
        ])) == 1


def get_number_of_different_ids(l):
    return len(set([x[3] for x in l]))
