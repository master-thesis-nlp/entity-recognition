from collections import defaultdict

from solvers.utils import IdentifierType, levenstein_distance
from solvers.modules import LocalPhraseBased, Pipe
from solvers.wikidatan.policies import FindIdInName2IdDict, PreferNativeAndLocal, AliasAddShortcuts, \
    NamedBasedOnLemmaAndTag, LastOrFirstNotNameWordMatch, OrAliasPolicy, AliasAddAllNamesRelatedToId


class Identifier:
    def __init__(self, repository, categories=None):
        self.repository = repository
        self.solvers = {
            "PER": self.get_id_by_label_for_persons,
            "ORG": self.get_id_by_label_for_organizations,
            "PRO": self.get_id_by_label_for_products,
            "LOC": self.get_id_by_label_for_locations,
            "EVT": self.get_id_by_label_for_events
        }
        if categories is None or categories == []:
            self.categories = set(list(self.solvers.keys()))
        else:
            self.categories = categories

    def flush(self):
        print("Identifier is Flushing")
        self.repository.flush()

    def __call__(self, lang, text, ners: IdentifierType) -> IdentifierType:
        len_at_begin = len(ners)
        ners_by_typpe = defaultdict(lambda: [])
        for entity, lemma, typpe, idd in ners:
            ners_by_typpe[typpe] += [(entity, lemma, typpe, idd)]
        ners = []
        for typpe, ner in ners_by_typpe.items():
            if typpe in self.solvers and typpe in self.categories:
                ners += self.solvers[typpe](lang, text, ner)
            else:
                if typpe in self.categories:
                    print("Unkown solver for type = ", typpe)
                # exit(1)
                ners += ner
        assert(len(ners) == len_at_begin)
        return ners

    def get_id_by_label_for_persons(self, lang, text, ners):
        p = Pipe([LocalPhraseBased(phrase_with_external_dict_policy=
                                   FindIdInName2IdDict(self.repository("PER"), PreferNativeAndLocal(self.repository))),
                  LocalPhraseBased(phrase_with_external_dict_policy=
                                   FindIdInName2IdDict(self.repository("PER"), PreferNativeAndLocal(self.repository)),
                                   phrase_in_phrase_policy=LastOrFirstNotNameWordMatch()),
                  LocalPhraseBased(phrase_with_external_dict_policy=
                                   NamedBasedOnLemmaAndTag("PER")),
                  LocalPhraseBased(phrase_with_external_dict_policy=
                                   NamedBasedOnLemmaAndTag("PER"),
                                   phrase_in_phrase_policy=LastOrFirstNotNameWordMatch())
                  ]
                 )
        return p(lang, ners)

    def get_id_by_label_for_locations(self, lang, text, ners):
        p = Pipe([LocalPhraseBased(phrase_with_external_dict_policy=
                                   FindIdInName2IdDict(self.repository("LOC"), PreferNativeAndLocal(self.repository))),
                  LocalPhraseBased(phrase_with_external_dict_policy=
                                   NamedBasedOnLemmaAndTag("LOC"))])
        return p(lang, ners)

    def get_id_by_label_for_events(self, lang, text, ners):
        p = Pipe([
            LocalPhraseBased(phrase_with_external_dict_policy=
                             FindIdInName2IdDict(self.repository("EVT"), PreferNativeAndLocal(self.repository))),
            LocalPhraseBased(phrase_with_external_dict_policy=
                             NamedBasedOnLemmaAndTag("EVT"))])
        return p(lang, ners)

    def get_id_by_label_for_products(self, lang, text, ners) -> list:
        typpe = "PRO"
        p = Pipe([LocalPhraseBased(phrase_with_external_dict_policy=
                                   FindIdInName2IdDict(self.repository(typpe), PreferNativeAndLocal(self.repository)),
                                   phrase_alias_policy=OrAliasPolicy([AliasAddAllNamesRelatedToId(self.repository, typpe), AliasAddShortcuts()])),
                  LocalPhraseBased(phrase_with_external_dict_policy=
                                   NamedBasedOnLemmaAndTag(typpe),
                                   phrase_alias_policy=AliasAddShortcuts())])
        return p(lang, ners)

    def get_id_by_label_for_organizations(self, lang, text, ners) -> list:  # typpe == ORG
        typpe = "ORG"
        p = Pipe([LocalPhraseBased(phrase_with_external_dict_policy=
                                   FindIdInName2IdDict(self.repository("ORG"), PreferNativeAndLocal(self.repository)),
                                   phrase_alias_policy=OrAliasPolicy([AliasAddAllNamesRelatedToId(self.repository, typpe), AliasAddShortcuts()])),
                  LocalPhraseBased(phrase_with_external_dict_policy=
                                   NamedBasedOnLemmaAndTag("ORG"),
                                   phrase_alias_policy=OrAliasPolicy([AliasAddAllNamesRelatedToId(self.repository, typpe), AliasAddShortcuts()]))])
        return p(lang, ners)


class Type1Identifier:
    def __call__(self, lang, text, ners: IdentifierType) -> IdentifierType:
        ners_by_typpe = defaultdict(lambda: [])
        for entity, lemma, typpe, idd in ners:
            ners_by_typpe[typpe] += [(entity, lemma, typpe, idd)]
        ners = []
        for typpe, ner in ners_by_typpe.items():
            p = LocalPhraseBased(phrase_with_external_dict_policy=
                                 NamedBasedOnLemmaAndTag(typpe),
                                 phrase_alias_policy=AliasAddShortcuts())
            ners += p(lang, ner)

        return ners


# ---- TESTING!!!!!!!!!!!!!!!!!

def merge_similar_phrases(ners, max_dist: int, ners_compare_metric, identifier_resolve_policy):
    visited = set()
    comp = []
    for node in range(len(ners)):
        if node not in visited:
            q = set([node])
            c = []
            while len(q):
                act = q.pop()
                c += [act]
                visited.add(act)
                for neigh in range(act + 1, len(ners)):
                    if neigh not in visited and levenstein_distance(ners[neigh][1], ners[act][1]) <= max_dist:
                        q.add(neigh)
            comp += [c]
    for c in comp:
        if len(c) > 0:
            idd = identifier_resolve_policy([ners[x][3] for x in c])
            for ind in c:
                phrase, lemma, typpe, idd_old = ners[ind]
                ners[ind] = (phrase, lemma, typpe, idd)
    return ners

# def get_number_of_different_ids(l):
#     return len(set([x[3] for x in l]))
#
#
# assert get_number_of_different_ids(
#     get_id_by_label_for_persons("pl", {}, [('Alexandra Nixa', 'Alexander Nix', 'PER', None),
#                                            ('Alexander Nix', 'Alexander Nix', 'PER', None),
#                                            ('Nixa', 'Nix', 'PER', None)])) == 1
# assert get_number_of_different_ids(
#     get_id_by_label_for_persons("pl", {},
#                                 [('Trump', 'Trump', 'PER', None),
#                                  ('Donalda Trumpa', 'Donald Trump', 'PER', None)])) == 1
# assert get_number_of_different_ids(get_id_by_label_for_persons("pl", {}, [('Trump', 'Trump', 'PER', None),
#                                                                           ('Donalda Trumpa Junior',
#                                                                            'Donald Trump Jr.', 'PER', None)])) == 1
# assert get_number_of_different_ids(get_id_by_label_for_persons("pl", {}, [
#     ('Belki', 'Belka', 'PER', None),
#     ('Marka', 'Marek', 'PER', None)])) == 2  # TODO should be one
# assert get_number_of_different_ids(
#     get_id_by_label_for_persons("pl", {}, [
#         ('XCSAD', 'XCSAD', 'PER', None),
#         ('Marka XCSAD', 'Marek XCSAD', 'PER', None)])) == 1
# assert get_number_of_different_ids(get_id_by_label_for_organizations("pl", {}, [
#     ("Unia Europejska", "Unia Europejska", "ORG", "None"),
#     ("UE", "UE", "ORG", "None"),
# ])) == 1
# assert get_number_of_different_ids(get_id_by_label_for_products("pl", {}, [
#     ('Sunday Timesa', 'Sunday Times', 'PRO', None), ('Sunday Times', 'Sunday Times', 'PRO', None),
#     ('Times', 'Times', 'PRO', None), ('Timesa', 'Times', 'PRO', None)
# ])) == 1
# assert get_number_of_different_ids(get_id_by_label_for_products("pl", {}, [
#     ('białe księgi', 'Białe księgi', 'PRO', None),
#     ('TVN24', 'TVN24', 'PRO', None)])) == 2
#
# assert get_number_of_different_ids(get_id_by_label_for_persons("ru", "", [
#     ('Терезой Мэй', 'Тереза Мэй', 'PER', None),
#     ('Мэй', 'Мэй', 'PER', None)
# ])) == 1
#
# assert get_number_of_different_ids(
#     merge_similar_phrases([("l", "leave.eu", "ORG", "ORG-leave.eu"), ("l", "leave.ue", "ORG", "ORG-leave.ue")], 1,
#                           ners_compare_metric=levenstein_ner_lemma_metric,
#                           identifier_resolve_policy=prefer_wikidata_id)) == 1
# assert get_number_of_different_ids(
#     merge_similar_phrases([("l", "leave.eu", "ORG", "ORG-leave.eu"), ("l", "leave.ue", "ORG", "Q12")], 1,
#                           ners_compare_metric=levenstein_ner_lemma_metric,
#                           identifier_resolve_policy=prefer_wikidata_id)) == 1
# assert get_number_of_different_ids(
#     merge_similar_phrases([("l", "Theresa May", "ORG", "Q23"), ("l", "Teresa May", "ORG", "Q12")], 1,
#                           ners_compare_metric=levenstein_ner_lemma_metric,
#                           identifier_resolve_policy=prefer_wikidata_id)) == 1
# assert get_number_of_different_ids(get_id_by_label_for_persons(
#     "ru", "", [
#         #         ('Борис Джонсон', 'Борис Джонсон', 'PER', None),
#         #         ('Бориса Джонсона', 'Борис Джонсон', 'PER', None),
#         #         ('Дэвид Дэвис', 'Дэвид Дэвис', 'PER', None),
#         ('Мэй', 'Мэй', 'PER', None),
#         ('Терезой Мэй', 'Тереза Мэй', 'PER', None),
#         ('Терезы Мэй', 'Тереза Мэй', 'PER', None)
#     ])) == 1

# assert get_number_of_different_ids(get_id_by_label_for_products("pl", {}, [('FT', 'FT', 'PRO', None),
#                                                                            ('Financial Times', 'Financial Times', 'PRO',
#                                                                             None)
#                                                                            ])) == 1
