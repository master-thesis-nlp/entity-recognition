import itertools
import pickle
import numpy as np
from solvers.utils import make_shrt, make_shrt_only_capital, generate_temporary_id, levenstein_distance
from solvers.wikidatan.repository import Repository


class FindIdInName2IdDict:
    def __init__(self, dic, choose_from_proposition_list_policy):
        self.word_to_id = dic
        self.choose_from_proposition_list = choose_from_proposition_list_policy

    def __call__(self, lang, entity, lemma) -> list:
        res = []
        if lemma in self.word_to_id:
            idd_ = self.choose_from_proposition_list(lang, self.word_to_id[lemma])
            res += [(lemma, idd_)]
        if entity in self.word_to_id:
            idd_ = self.choose_from_proposition_list(lang, self.word_to_id[entity])
            res += [(entity, idd_)]
        return res


class LastOrFirstNotNameWordMatch:
    def __init__(self):
        names_p = pickle.load(open("application_data/wikidata/names.pickle", "rb"))
        self.names = set()
        for idd, values in names_p.items():
            for l in ["pl", "cs", "ru", "en", "bg"]:
                for x in values.get("l_pl", []):
                    self.names.add(x)

    def __call__(self, lang, phrase, phrase2):
        p1 = phrase.split(" ")
        p2 = phrase2.split(" ")
        res = p1[-1] == p2[-1]
        if p1[0] not in self.names and p2[0] not in self.names:
            res = res or p1[0] == p2[0]
        return res


class OrPolicy:
    def __init__(self, policies):
        self.policies = policies

    def __call__(self, *args):
        for p in self.policies:
            if p(*args):
                return True
        return False


class OneMistakeByWord:
    def __call__(self, lang, phrase, phrase2):
        dst = levenstein_distance(phrase, phrase2)
        res = 2 * dst / (phrase.count(" ") + phrase2.count(" ") + 2) <= 1.0
        return res


class NamedBasedOnLemmaAndTag:
    def __init__(self, pref):
        self.prefix = pref

    def __call__(self, lang, entity, lemma) -> list:
        idd = generate_temporary_id(self.prefix, lemma)
        return [(lemma, idd), (entity, idd)]


class LastWordMatch:
    def __call__(self, phrase, phrase2):
        return phrase.split(" ")[-1] == phrase2.split(" ")[-1]


class PreferNativeAndLocal:
    def __init__(self, repository):
        self.repository = repository

    def __call__(self, lang, proposition_list):
        if type(proposition_list) != tuple and type(proposition_list) != list:
            return proposition_list
        local = list(
            filter(lambda x: x[1] == lang, proposition_list))
        if len(local) > 0:
            prs = [self.repository.get_pagerank_for(idd) for idd, lang in local]
            ind = np.argmax(prs, axis=0)
            return local[ind][0]
        else:
            prs = [self.repository.get_pagerank_for(idd) for idd, lang in proposition_list]
            ind = np.argmax(prs, axis=0)
            return proposition_list[ind][0]


class PreferLocal:
    def __call__(self, lang, proposition_list):
        if type(proposition_list) != tuple and type(proposition_list) != list:
            return proposition_list
        local = list(
            filter(lambda x: x[1] == lang, proposition_list))
        if len(local) > 0:
            return local[0][0]
        else:
            return proposition_list[0][0]


class AliasAddShortcuts:
    def __call__(self, ret) -> list:
        res = []
        for name, idd in ret:  # TODO replace with foreach over names of idd
            res += [(name, idd)]
            shrt = make_shrt(name)
            if len(shrt) > 1:
                res += [(shrt, idd)]
            shrt = make_shrt_only_capital(name)
            if len(shrt) > 1:
                res += [(shrt, idd)]
        return res


class AliasAddAllNamesRelatedToId:
    def __init__(self, repo: Repository, typpe: str):
        self.repo = repo
        self.typpe = typpe

    def __call__(self, ret) -> list:
        res = []
        for name, idd in ret: #TODO uncomment today!!!!!!
            res += itertools.product(self.repo.get_names_for_id(idd, self.typpe), [idd])
        return res


class OrAliasPolicy:
    def __init__(self, alias_policies: list):
        self.alias_policies = alias_policies

    def __call__(self, ret) -> list:
        res = []
        for p in self.alias_policies:
            res += p(ret)
        return res
