import os
import pickle
from collections import defaultdict
from functools import lru_cache
from random import random, choice

import psutil
import pygtrie as trie

import gc
from memory_profiler import profile
from mezmorize import Cache

import fasttext_decorator
from solvers.utils import OwnTrieDict

cache = Cache(CACHE_TYPE='filesystem', CACHE_DIR='cache')


class Repository:
    def __init__(self, one_file_at_the_time: bool = False, true_native=False, preload_dictionaries=False):
        self.one_file_at_the_time = one_file_at_the_time
        self.memory = {}
        self.langs = ["en", "pl", "cs", "bg", "ru"]
        self.repository_provider = {
            "PRO": self.get_prod,
            "PER": self.get_people,
            "ORG": self.get_org,
            "EVT": self.get_evt,
            "LOC": self.get_loc,
        }
        self.fasttext = {}
        self.true_native = true_native
        page_rank = pickle.load(open("application_data/wikidata/page_rank.pickle", "rb"))
        page_rank2 = pickle.load(open("application_data/wikidata/page_rank1.pickle", "rb"))
        self.page_rank = {**page_rank, **page_rank2}
        if preload_dictionaries:
            for typpe in self.repository_provider.keys():
                self.memory[typpe] = self.load(typpe)

    @lru_cache(maxsize=25600)
    def get_names_for_id(self, idd, typpe):
        res = []
        for name, values in self.__call__(typpe).items():
            for idd_, l in values:
                if idd_ == idd:
                    res += [name]
                    break
        return res

    def get_fasttext_for_lang(self, lang):
        if lang not in self.fasttext:
            mem_avail_gib = psutil.virtual_memory().available / 1024 / 1024 / 1024
            while len(self.fasttext) > 0 and mem_avail_gib < 5:
                d = choice(list(self.fasttext.keys()))
                self.fasttext.pop(d)
                mem_avail_gib = psutil.virtual_memory().available / 1024 / 1024 / 1024
                print("removing element from fasttext cache")

            gc.collect()
            self.fasttext[lang] = fasttext_decorator.load_languages([lang])[0]

        return self.fasttext[lang]

    # @profile
    def flush(self):
        # for x in self.memory.keys():
        #     del self.memory[x]
        # self.memory = {}
        pass

    def get_pagerank_for(self, ind):
        if type(ind) == str:
            if ind[0] == "Q":
                ind = int(ind[1:])
            else:
                return 0
        return self.page_rank[ind] if ind in self.page_rank else 0

    def __call__(self, typpe: str) -> OwnTrieDict:
        if typpe not in self.memory:
            if self.one_file_at_the_time:
                self.memory = {}
            if typpe not in self.repository_provider:
                print("Missing provider for ", typpe)
                print("Got only ", self.repository_provider.keys())
            self.memory[typpe] = self.load(typpe)
        return self.memory[typpe]

    # @profile
    # @cache.memoize()
    def load(self, typpe):
        cache_file = "application_data/wikidata/cached."+typpe+".pickle"
        try:
            with open(cache_file, "rb") as file:
                res = pickle.load(file)
                print("using recalculated wikidata file - threshold updates won't be applied. Remove ", cache_file, " to apply them again")
                return res
        except (ValueError, FileNotFoundError, EOFError):
            print("Load ", typpe)
            res = OwnTrieDict(self.repository_provider[typpe]())# without owntriedict - 5465 mb
            with open(cache_file, "wb") as file:
                pickle.dump(res, file)
            return res

    # @profile
    # @cache.memoize()
    def get_people(self, min_pr = 3):
        d = defaultdict(list)
        human_list = pickle.load(open("application_data/wikidata/humans.pickle", "rb"))
        d = self.add_labels_to_dict(d, human_list, min_pr)
        return dict(d)

    # @cache.memoize()
    def get_org(self, min_pr = 9) -> dict:
        d = defaultdict(list)
        org_list = pickle.load(open("application_data/wikidata/organizations.pickle", "rb"))
        d = self.add_labels_to_dict(d, org_list, min_pr)
        for idd, value in org_list.items():
            pr = self.get_pagerank_for(idd)
            if pr > min_pr:
                for name in value.get("official_name", []):  # TODO losing language information
                    d[name.lower()] += [(idd, None)]
        return dict(d)

    # @cache.memoize()
    def get_prod(self, min_pr = 15) -> dict:
        d = defaultdict(list)
        prod_list = pickle.load(open("application_data/wikidata/products.pickle", "rb"))
        d = self.add_labels_to_dict(d, prod_list, min_pr)
        return dict(d)

    # @cache.memoize()
    def get_loc(self, min_pr = 25) -> dict:
        d = defaultdict(list)
        loc_list = pickle.load(open("application_data/wikidata/locations.pickle", "rb"))
        d = self.add_labels_to_dict(d, loc_list, min_pr)
        return dict(d)

    # @cache.memoize()
    def get_evt(self, min_pr = 4) -> dict:
        d = defaultdict(list)
        loc_list = pickle.load(open("application_data/wikidata/events.pickle", "rb"))
        d = self.add_labels_to_dict(d, loc_list, min_pr) # 5 - 0.730769
        return dict(d)

    def add_labels_to_dict(self, d, list, min_pr=100):
        langs = ['pl', 'cs', 'en', 'ru']
        i = 0
        for idd, value in list.items():
            int_idd = int(idd[1:])
            pr = self.get_pagerank_for(int_idd)
            if pr > min_pr:
                i += 1
                if self.true_native:
                    native = value.get('nat', None)
                else:
                    native = None
                for l in langs:  # labels
                    label = "l_" + l
                    if label in value:
                        if type(value[label]) == tuple or type(value[label]) == list:
                            for name in value.get("l_" + l, []):
                                is_native = (name in native) if native is not None else False
                                d[name.lower()] += [(idd, l)]
                        else:
                            is_native = (value[label] in native) if native is not None else False
                            d[value[label].lower()] += [(idd, l)]
        print("Loaded ", i, "/", len(list), " [", int(i/len(list)*100),"%] with pr = ", min_pr)
        return d
