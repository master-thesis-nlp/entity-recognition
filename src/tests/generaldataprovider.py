from unittest import TestCase

from bsnlp2019.data_provider import BSNLPBlindTest
from bsnlp2019.general_data_provider import GeneralDataProvider


class TestAddHeuristics(TestCase):
    def test_blind_test_migration(self):
        # Given
        testset = ["ryanair"]#, "nord_stream"]
        langs = ["pl"]#, "ru", "bg", "cs"]
        # When
        for l in langs:
            for t in testset:
                old = BSNLPBlindTest([t], [l])
                n = GeneralDataProvider([t], [l], "/media/bartek/ADDITIONAL/kopie/praca_magisterska_after_bsnlp_1/bsnlp2019/")
                # Then
                old_r = sorted(list(old(l))[0][1])
                new_r = sorted(list(n(l))[0][1])
                self.assertListEqual(old_r, new_r)
