from unittest import TestCase

from solvers.heuristics.ners import add_new_or_same_sub_ners


class TestAddHeuristics(TestCase):
    def test_add_new_or_same_sub_ners_preserve_old(self):
        inp = match_format([(1, 2)], "PRO")
        inp2 = match_format([], "PRO")

        ans = add_new_or_same_sub_ners(inp, inp2)
        ans2 = add_new_or_same_sub_ners(inp2, inp)

        self.assertEqual(inp, ans)
        self.assertEqual(inp, ans2)

    def test_add_new_or_same_sub_ners_sub_entity(self):
        inp = match_format([(1, 5)], "PRO")
        inp2 = match_format([(1, 2)], "PRO")

        ans = add_new_or_same_sub_ners(inp, inp2)
        ans2 = add_new_or_same_sub_ners(inp2, inp)

        self.assertEqual(set(inp + inp2), set(ans))
        self.assertEqual(set(inp + inp2), set(ans2))

    def test_add_new_or_same_sub_ners_sub_entity_with_diffferent_cat(self):
        inp = match_format([(1, 5)], "PRO")
        inp2 = match_format([(1, 2)], "LOC")

        ans = add_new_or_same_sub_ners(inp, inp2)
        ans2 = add_new_or_same_sub_ners(inp2, inp)

        self.assertEqual(set(inp), set(ans))
        self.assertEqual(set(inp2), set(ans2))


def match_format(arr, type):
    return [(x, type) for x in arr]
