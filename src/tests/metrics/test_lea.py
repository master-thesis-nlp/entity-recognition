from itertools import product
from unittest import TestCase

from metrics.lea import generate_links, get_metric


class TestGenerateLinks(TestCase):
    def test_generate_links_one_pair(self):
        exp = [("x", "y", "x1")]
        ans = generate_links([("x", "x", "x", "x1"), ("y", "y", "y", "x1")])
        self.assertEqual(exp, ans)

    def test_generate_links_three_pairs(self):
        inp = match_format(["x", "y", "z"], "x1")
        exp = [("x", "y", "x1"), ("x", "z", "x1"), ("y", "z", "x1")]
        ans = generate_links(inp)
        self.assertEqual(exp, ans)

    def test_generate_links_singleton(self):
        exp = [("x", "x", "x1")]
        ans = generate_links([("x", "x", "x", "x1")])
        self.assertEqual(exp, ans)

    def test_generate_links_for_empty(self):
        exp = []
        ans = generate_links([])
        self.assertEqual(exp, ans)

    def test_generate_links_mixed(self):
        exp = [("x", "y", "x1"), ("z", "z", "z1")]
        ans = generate_links([("x", "x", "x", "x1"), ("y", "y", "y", "x1"), ("z", "z", "z", "z1")])
        self.assertEqual(exp, ans)

    def test_generate_links_for_same_person(self):
        inp = [('Tusk', 'Tusk', 'PER', 'PER-Donald-Tusk')] * 6
        exp = [('Tusk', 'Tusk', 'PER-Donald-Tusk')]

        ans = generate_links(inp)

        self.assertEqual(exp, ans)

    def test_generate_links_example_from_paper(self):
        phrasek1 = ["a", "b", "c"]
        phrasek2 = ["d", "e", "f", "g"]
        phraser1 = ["a", "b"]
        phraser2 = ["c", "d"]
        phraser3 = ["f", "g", "h", "i"]
        k1 = match_format(phrasek1, "k1")
        k2 = match_format(phrasek2, "k2")
        r1 = match_format(phraser1, "r1")
        r2 = match_format(phraser2, "r2")
        r3 = match_format(phraser3, "r3")

        expk1 = 3
        expk2 = 6
        expr1 = 1
        expr2 = 1
        expr3 = 6

        ansk1 = len(generate_links(k1))
        ansk2 = len(generate_links(k2))
        ansr1 = len(generate_links(r1))
        ansr2 = len(generate_links(r2))
        ansr3 = len(generate_links(r3))

        self.assertEqual(expk1, ansk1)
        self.assertEqual(expk2, ansk2)
        self.assertEqual(expr1, ansr1)
        self.assertEqual(expr2, ansr2)
        self.assertEqual(expr3, ansr3)

    def test_get_metric_no_match_results_0(self):
        s1 = [tuple(["y"] * 4)]
        s2 = [tuple(["x"] * 4)]
        exp = 0
        ans = get_metric(s1, s2)
        self.assertEqual(exp, ans)

    def test_get_metric_with_same_results(self):
        s1 = [tuple(["x"] * 4)]
        s2 = [tuple(["x"] * 4)]
        exp = 1
        ans = get_metric(s1, s2)
        self.assertEqual(exp, ans)

    def test_get_metric_with_same_results_mixed(self):
        s1 = [("x", "x", "x", "y")] * 5 + [("a", "a", "a", "y")] * 11
        s2 = [("x", "x", "x", "x")] * 5 + [("a", "b", "c", "x")] * 10
        exp = 1
        ans = get_metric(s1, s2)
        self.assertNotEqual(exp, ans)

    def test_get_metric_example_from_paper(self):
        # Arrange
        k1 = match_format(["a", "b", "c"], "k1")
        k2 = match_format(["d", "e", "f", "g"], "k2")
        r1 = match_format(["a", "b"], "r1")
        r2 = match_format(["c", "d"], "r2")
        r3 = match_format(["f", "g", "h", "i"], "r3")
        exp = 5 / 21  # From paper
        exp2 = 1 / 3  # Calculated by hand
        # Act
        ans = get_metric(k1 + k2, r1 + r2 + r3)
        ans2 = get_metric(r1 + r2 + r3, k1 + k2)
        # Assert
        self.assertEqual(exp, ans)
        self.assertEqual(exp2, ans2)


def match_format(arr, key):
    return [(x, x, x, key) for x in arr]
