import itertools

from matplotlib import pyplot as plt
from numpy import mean
import numpy as np
from collections import defaultdict


def plot_results_for(lang, **kwargs):
    plt.rcParams["figure.figsize"] = (10, 5)
    plt.ylabel("tests")
    plt.xlabel("score")
    plt.title("Accuracy for " + lang)
    colors = itertools.cycle(['red', 'green', 'blue'])
    for (name, value), color in zip(kwargs.items(), colors):
        val_not_none = list(filter(lambda x: x is not None, value))
        prec_avg = mean(val_not_none)
        plt.hlines(y=prec_avg, xmin=0, xmax=len(value), color=color, linestyles='dotted',
                   label=name + " average " + str(prec_avg))
        plt.plot(value, label=name, color=color)

    plt.legend()
    plt.show()


def summarize_results(title, data, diff=None):
    if diff is None:
        methods = len(set([x[2] for x in data]))
        diff = 2 if methods > 1 else 1
    data = [title] + sorted(data)
    prev = data[0][0:diff]
    print()
    for items in data:
        if items[0:diff] != prev:
            print("--------------------------------------------------------------")
        prev = items[0:diff]
        for item in items:
            if type(item) == float:
                row_format = "{:<7f} |"
            elif type(item) == int:
                row_format = "{:<8d} |"
            else:
                row_format = "{:<8s} |"
            if item is None:
                item = "None"
            print(row_format.format(item), end='')
        print()


def visualize_multiple_categories(res: list, title = "Average efficency of method = "):
    methods = list(set([x[2] for x in res[1:]]))
    for m in methods:
        visualize_all_categories(m, res[1:], default_header=title)


def visualize_all_categories(method, res, default_header: str = "Average efficency of method = ", plot_width=15):
    d = defaultdict(dict)
    d2 = defaultdict(lambda: defaultdict(list))
    categories = []
    for lang, cat, m, f1, p, p_num, r, r_num, metric in res:
        if method == m:
            d[lang][cat] = (f1, p, r)
            d2[lang][cat] += [f1 if f1 else 0]
            categories += [cat]
    categories = sorted(list(set(categories)))

    x = np.arange(len(categories))  # the label locations
    width = 0.92  # the width of the bars
    bar_margin = (1 - width) / 2
    bar_width = width / len(d2)

    fig, ax = plt.subplots()
    fig.set_size_inches(plot_width, 6)
    rects = []
    for ind, (lang, dx) in enumerate(d2.items()):
        v = [0] * len(categories)
        for cat, values in dx.items():
            # print("Ind: ", )
            v[categories.index(cat)] = values[0]
        rects += [ax.bar(x - width / 2 + ind * bar_width + bar_width / 2, v, bar_width - 0.03, label=lang)]

    # Add some text for labels, title and custom x-axis tick labels, etc.
    ax.set_ylabel('F1 score')
    ax.set_title(default_header + method)
    ax.set_xticks(x)
    ax.set_xticklabels(categories)
    ax.legend()

    def autolabel(rects):
        """Attach a text label above each bar in *rects*, displaying its height."""
        for rect in rects:
            height = rect.get_height()
            ax.annotate('{0:.2f}'.format(height),
                        xy=(rect.get_x() + rect.get_width() / 2, height),
                        xytext=(0, 3),  # 3 points vertical offset
                        textcoords="offset points",
                        ha='center', va='bottom')

    for r in rects:
        autolabel(r)

    fig.tight_layout()

    plt.show()


def visualize_compare_methods(lang, res, selected_methods=None, plot_width=16, title="Language = "):
    if selected_methods is None:
        res2 = [[x[2], x[1], x[0]] + list(x[3:]) for x in res]
    else:
        res2 = [[x[2], x[1], x[0]] + list(x[3:]) for x in res if x[2] in selected_methods]
        print(res2)
    visualize_all_categories(lang, res2, title)
